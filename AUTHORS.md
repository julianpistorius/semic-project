Credits
=======

Project Lead
----------------

* Mario Krapp <mario.krapp@pik-potsdam.de>

Project Contributors
------------

* Alexander Robinson (co-author of paper) <robinson@fis.ucm.es>
* Andrey Ganopolski (co-author of paper; project PI) <andrey.ganopolski@pik-potsdam.de>
