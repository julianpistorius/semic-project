import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import dates
import numpy as np
from scipy import integrate
from scipy.stats.stats import pearsonr
from datetime import date, timedelta
import glob
import pickle
import sys
sys.path.append('src/tools')
import netCDF4 as nc4
from Greenland import GreenlandMap
from taylorDiagram import TaylorDiagram
from mpl_toolkits.axes_grid1.axes_divider import HBoxDivider
import mpl_toolkits.axes_grid1.axes_size as Size


def make_heights_equal(fig, rect, ax1, ax2, pad):
    # pad in inches

    h1, v1 = Size.AxesX(ax1), Size.AxesY(ax1)
    h2, v2 = Size.AxesX(ax2), Size.AxesY(ax2)

    pad_v = Size.Scaled(1)
    pad_h = Size.Fixed(pad)

    my_divider = HBoxDivider(fig, rect,
                             horizontal=[h1, pad_h, h2],
                             vertical=[v1, pad_v, v2])

    ax1.set_axes_locator(my_divider.new_locator(0))
    ax2.set_axes_locator(my_divider.new_locator(2))

def get_lonlat(nc):
    lat = np.array(nc.variables['LAT'][:]).squeeze()
    lon = np.array(nc.variables['LON'][:]).squeeze()
    return lon,lat

def read_data(nc,*vars):
    data = []
    for v in vars:
        x = np.array(nc.variables[v][:]).squeeze()
        data.append(x)
    return data

def draw_diurnal_cycle(label="(a)"):
    print "=================="
    print "Draw diurnal cycle"
    print "=================="
    mpl.rcParams['font.size'] = 16

    x = np.arange(0.0,24.1,0.01)

    def triangle(mean,amp):
        m = 2.*amp/12.
        y = m*x+mean-amp
        y = np.where(x>12.,-m*x+mean+3.*amp,y)
        return y

    f = plt.figure(figsize=(10,5))
    ax1 = plt.subplot(121)
    def temp(amp,mean,plot=False,output=False):
        # roots of cosine function
        x1 = 12./np.pi*(np.arccos(mean/amp))
        x2 = 12./np.pi*(2.*np.pi-np.arccos(mean/amp))
        func = lambda x: -amp*np.cos(np.pi/12.*x)+mean
        f1 = integrate.quad(func, 0., x1)[0]
        f2 = integrate.quad(func, x1, 12.)[0]
        above = 2.*f2
        below = -2.*f1
        total = above + below
        above /= total
        below /= total

        # roots of triangle function
        x1t = 6.*(amp - mean)/amp
        print (mean+amp)/2., (mean-amp)/2.

        # the positive and negative mean values
        dt = 2.*x1#24. - 24./pi*(arccos(mean/amp))
        Tbelow = 24./np.pi*(mean*np.arccos(mean/amp)-amp*np.sqrt(1.-mean**2/amp**2))/dt
        dt = x2-x1#2.*(24. - 12./pi*(2.*pi-arccos(mean/amp)))
        Tabove = 24./np.pi*(-mean*np.arccos(mean/amp)+amp*np.sqrt(1.-mean**2/amp**2)+np.pi*mean)/dt

        if plot:
            ax1.plot(x,-amp*np.cos(np.pi/12.*x)+mean,lw=4,label=r'$A$=%.1f and $T_s$=%.1f'%(amp,mean))
            ax1.xaxis.set_ticks([0,3,6,9,12,15,18,21,24])
            color = ax1.lines[-1].get_color()
            idx1 = (abs(-amp*np.cos(np.pi/12.*x[:len(x)/2])+mean-Tabove)).argmin()
            idx2 = (abs(-amp*np.cos(np.pi/12.*x[:len(x)/2])+mean-Tbelow)).argmin()
            ax1.axhline(Tabove,xmin=x[idx1]/24.,xmax=x[-idx1-4]/24.,color=color,lw=2,ls='--')
            ax1.axhline(Tbelow,xmin=0.0,xmax=x[idx2]/24.,color=color,lw=2,ls='-')
            ax1.axhline(Tbelow,xmin=x[-idx2]/24.,xmax=1.,color=color,lw=2,ls='-')
            # fill area below and above 0degC
            ax1.fill_between(x,-amp*np.cos(np.pi/12.*x)+mean,alpha=0.1,facecolor='black')
            # roots of cosine
            ax1.plot(x1,0.,'o',ms=12,color=color)
            ax1.plot(x2,0.,'o',ms=12,color=color)
        if output:
            print 'A=%.1f'%amp+' m=%.1f'%mean+'| above: %.2f'%above + ', below: %.2f'%below + ', T+: %.2f'%Tabove + ', T-: %.2f'%Tbelow
        return Tabove, Tbelow


    temp(5.0,0.0,plot=True,output=True)
    temp(5.0,-3.,plot=True,output=True)

    ax1.grid()
    ax1.axhline(0.0,color='k')
    ax1.set_xlim(0.,24.)
    ax1.set_ylim(-8.,7.)
    ax1.set_xlabel('hours')
    ax1.set_ylabel('temperature')
    leg = plt.legend(loc=2,prop={'size':16})
    leg.get_frame().set_alpha(0.5)
    #plt.tight_layout()
    #f.savefig('reports/figures/diurnalcycle.pdf',bbox_inches='tight', pad_inches = 0.0)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""
    print "======================"
    print "Draw diurnal cycle map"
    print "======================"

    nc = nc4.Dataset('data/interim/diurnal_cycle.nc','r')

    temp = np.array(nc.variables['TTMAX'][:]).squeeze()[2,:,:] # JJA
    temp = np.where(temp<0.,np.nan,temp)
    temp_m = np.ma.masked_where(np.isnan(temp),temp)/2.
    lon,lat = get_lonlat(nc)
    nc.close()

    #f = plt.figure(figsize=(6,8))

    ax2 = plt.subplot(122)
    m = GreenlandMap(lon, lat)#,lat_0=71.8,lon_0=-41)
    m.drawcoastlines().set_alpha(0.5)

    x,y = m(lon,lat)
    im = m.pcolormesh(x,y,temp_m,vmin=0.0,vmax=6.0,cmap='jet',rasterized=True)
    cbaxes = f.add_axes([1., 0.15, 0.02, 0.75]) 
    plt.colorbar(im, cax=cbaxes,label='temperature [K]')
    co = m.contour(x,y,temp_m,np.arange(2.,5.01,1.),colors='k',ax=ax2)
    plt.clabel(co,inline=1, fontsize=15,fmt='%1.1f')

    # label
    ax1.text(-0.1,1.0,"(a)",fontsize=20,transform=ax1.transAxes)
    ax2.text(-0.2,1.0,"(b)",fontsize=20,transform=ax2.transAxes)
    make_heights_equal(f, 111, ax1, ax2, pad=0.5)
    plt.tight_layout()
    f.savefig('reports/figures/fig01.pdf',bbox_inches='tight', pad_inches = 0.02)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

def draw_region_mask():
    print "================"
    print "Draw region mask"
    print "================"

    fnm = 'data/interim/forcing_1990-1999.nc'
    nc = nc4.Dataset(fnm,'r')
    region_land = np.array(nc.variables['MSK'][:]).squeeze()-1
    ice_margin = np.array(nc.variables['SOL'][:]).squeeze()
    lon,lat = get_lonlat(nc)

    colors = ['white','goldenrod','red','forestgreen','dodgerblue']

    lscm = mpl.colors.LinearSegmentedColormap
    mycm = lscm.from_list('mycm',colors)

    f = plt.figure(figsize=(6,8))
    plt.subplot(111)

    m = GreenlandMap(lat,lon)
    x,y = m(lon,lat)
    m.drawcoastlines().set_alpha(0.6)
    m.pcolormesh(x,y,region_land,cmap=mycm,vmin=0,vmax=4,rasterized=True)
    co = m.contour(x,y,ice_margin,np.arange(11.5,12.5,0.5),linewidths=2,colors='k')
    plt.figtext(0.34,0.36,'1',size=24,weight='bold')
    plt.figtext(0.52,0.6,'3',size=24,weight='bold')
    plt.figtext(0.41,0.25,'2',size=24,weight='bold')
    plt.figtext(0.72,0.54,'land',size=24,weight='bold')
    plt.tight_layout()
    f.savefig('reports/figures/fig02.pdf', format='PDF', bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

def draw_taylor(period,label,legend=True):
    print "==================="
    print "Draw Taylor Diagram"
    print "==================="

    mpl.rcParams['font.size'] = 14

    fnm = './data/raw/semic_%s_opti_ts.nc' % period
    nc  = nc4.Dataset(fnm,'r')

    var_names = ['Surface Temperature (K)','Surface Mass Balance (mm/day)','Surface Melt (mm/day)']

    var_names = ['TS','SW','SMB','ME']

    data = np.array(read_data(nc,'tsurf_ts','swnet_ts','smb_ts','melt_ts')).squeeze()
    orig = np.array(read_data(nc,'tsurf_mar_ts','swnet_mar_ts','smb_mar_ts','melt_mar_ts')).squeeze()
    nc.close()

    print np.shape(data), np.shape(orig)

    nsize = len(data[:,0,0])
    bsize = len(data[0,0,:])
    corcoef = np.zeros((nsize))
    stddev = np.zeros((nsize))

    f = plt.figure(figsize=(8,6))
    dia = TaylorDiagram(1., fig=f, rmax=1.55)
    colors = ['goldenrod','red','forestgreen','dodgerblue','grey','purple','orange','magenta','sandybrown']
    markers = ['o','s','^','v','o']

    for b in range(bsize):
        reg = str(b)#'(basin %d)' % b
        if b == 0: reg = 'land'
        for i in range(nsize):
            orig_anom = orig[i,:,b]-np.average(orig[i,:,b])
            ref = np.std(orig_anom)
            opti_anom = data[i,:,b]-np.average(data[i,:,b])
            stddev[i] = np.std(opti_anom/ref)
            corcoef[i] = pearsonr(orig_anom/ref,opti_anom/ref)[0]
            dia.add_sample(stddev[i], corcoef[i], ls='', marker=markers[i], markersize=8,
                           color=colors[b],label=var_names[i]+' '+reg,alpha=0.75)
    #         print b, var_names[i], stddev[i], corcoef[i]
    contours = dia.add_contours(colors='0.5',levels=6)
    if legend: plt.legend(loc=9,numpoints=1,ncol=bsize,fontsize=12,framealpha=0.5,bbox_to_anchor=(0.5, -0.1))
    plt.clabel(contours, inline=1, fontsize=14)
    plt.text(1,1.4,period,fontsize=20)
    plt.text(-0.2,1.5,label,fontsize=20)
    f.savefig('reports/figures/taylor_diagram_%s.pdf'%period, type='PDF', bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""


def draw_basin_time_series(period,figname):
    print "==============================="
    print "Draw Basin-averaged time series"
    print "==============================="

    fnm = './data/raw/semic_%s_opti_ts.nc' % period
    nc  = nc4.Dataset(fnm,'r')

    var_names = [r'$T_s$',r'$SW_\mathrm{net}$',r'$SMB$',r'$\hat{h_s}$',r'$M$',r'$R$',r'$H_L$',r'$H_S$']

    mar = np.array(read_data(nc,'tsurf_mar_ts','swnet_mar_ts','smb_mar_ts','hsnow_mar_ts',\
                             'melt_mar_ts','refr_mar_ts','lhf_mar_ts','shf_mar_ts')).copy()
    semic = np.array(read_data(nc,'tsurf_ts','swnet_ts','smb_ts','hsnow_ts',\
                               'melt_ts','refr_ts','lhf_ts','shf_ts')).copy()
    nc.close()
    blen = len(semic[0,0,:])
    nvar = len(semic[:,0,0])

    ntime = semic.shape[1]
    time = np.arange(ntime)/(ntime/(nyears*12.))

    def plot_model_vs_mar(ax,semic,mar,vid,bid):
        if vid in [2,4,5]:
            fac = 86.4e6
        else:
            fac=1

        x1 = fac*semic[vid,:,bid]
        x2 = fac*mar[vid,:,bid]
        if vid == 3:
            x1 = (x1 - np.mean(x1))/np.std(x1)
            x2 = (x2 - np.mean(x2))/np.std(x2)
        ax.plot(time,x1,'k-',lw=1,label='SEMIC',alpha=0.6,rasterized=True)
        ax.plot(time,x2,'r-',lw=1,label='MAR/CanESM2',alpha=0.6,rasterized=True)

        x1_avg = np.average(x1)
        x1_std = np.std(x1)
        x2_avg = np.average(x2)
        x2_std = np.std(x2)
        crmse = np.sqrt(np.sum(((x1 - x1_avg)-(x2 - x2_avg))**2)/len(time))/x2_std
        res = np.sqrt(crmse**2 + ((x1_avg-x2_avg)/x2_std)**2)
        if vid == 7: ax.set_xlabel('time')
        if bid == 0: ax.set_ylabel(var_names[vid])
        ax.set_title(r'$E$ = %4.2f'%res,loc='left',fontsize=14)
        if vid == 0:
            if bid == 0:
                ax.set_title('land',loc='right')
            else:
                ax.set_title('region '+' '+str(bid),loc='right')
        if (vid == 7) and (bid == 0): ax.legend(loc=2,ncol=2,fontsize=14,bbox_to_anchor=(0.0, -0.4))
        alldata = np.append(fac*mar[vid,:,:],fac*semic[vid,:,:])
        minx = np.amin(alldata)
        maxx = np.amax(alldata)
        if vid != 3:
            ax.set_ylim(minx,maxx)
        else:
            ax.set_ylim(-3,3)
        #ax.set_xlim(time[0],time[-1])
        ax.set_xlim(0,nyears*12)
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels)

    xticks = range(0,int(nyears*12+1),60)
    xticklabels = [int(period[:4])+x/12 for x in xticks]

    mpl.rcParams['figure.figsize'] = 13,18
    mpl.rcParams['font.size'] = 14
    f, axarr = plt.subplots(nvar,blen)

    x = 0
    for i in range(nvar):
        y = 0
        for j in range(blen):
            plot_model_vs_mar(axarr[x,y],semic,mar,i,j)
            y += 1
        x += 1
    plt.tight_layout()
    f.savefig('reports/figures/%s'%figname, type='PDF', bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""


def draw_cumulative_time_series(period,labels,legend=True):
    print "================================="
    print "Draw cumulative basin time series"
    print "================================="

    fnm = './data/raw/semic_%s_opti_ts.nc' % period
    nc  = nc4.Dataset(fnm,'r')

    area = np.array([531875, 253125, 725000, 825000])*1.e6
    print sum(area)
    print area/np.sum(area)
    var_names = ['Surface Mass Balance','Surface Melt']

    semic = np.array(read_data(nc,'smb_ts','melt_ts')).squeeze()
    mar = np.array(read_data(nc,'smb_mar_ts','melt_mar_ts')).squeeze()
    nc.close()

    #colors = ['black','dodgerblue','red']

    print semic.shape

    ntime = semic.shape[1]

    time = np.arange(ntime)/(ntime/(nyears*12.))

    total_ice_semic = np.zeros(np.shape(semic[:,:,0]))
    total_ice_mar = np.zeros(np.shape(semic[:,:,0]))
    def plot_model_vs_mar(ax,bid):
        fac = area[bid]*1.e3*86400.*1.e-12 # m2*kg/m3*s/day*Gt/kg= s/m*Gt/day
        ax.axhline(y=0.,color='k')
        for vid in range(len(var_names)):
            if bid != 0:
                total_ice_semic[vid,:] += np.cumsum(fac*semic[vid,:,bid])
                total_ice_mar[vid,:] += np.cumsum(fac*mar[vid,:,bid])
            l, = ax.plot(time,np.cumsum(fac*semic[vid,:,bid]),lw=3,label=var_names[vid]+'\nSEMIC')
            ax.plot(time,np.cumsum(fac*mar[vid,:,bid]),ls='-',c=l.get_color(),lw=3,alpha=0.5,label='MAR/CanESM2')
            semic_val = np.cumsum(fac*semic[vid,:,bid])[-1]
            mar_val = np.cumsum(fac*mar[vid,:,bid])[-1]
            print "Basin %d %s %3.1f (%3.1f%%)" % (bid, var_names[vid], semic_val - mar_val,
                                                   (semic_val - mar_val)/semic_val*100.)
        if bid == 3:
            if legend: ax.legend(loc=2,ncol=2,bbox_to_anchor=(0.15, -0.3),borderaxespad=0.)
        ax.grid()
        if bid == 0:
            ax.set_title('land')
        else:
            ax.set_title('region '+str(bid))
        if bid in [2,3]: ax.set_xlabel(xlabel)
        ax.set_xlim(0,nyears*12)
        if bid in [1,2]:
            ax.set_ylim(-7500,11000)
        else:
            ax.set_ylim(-500,3200)
        ax.set_xticks(xticks)
        ax.set_xticklabels(xticklabels)
        #if bid in [0,3]:
        ax.set_ylabel('cum. mass [Gt]')

    mpl.rcParams['figure.figsize'] = 10,8
    mpl.rcParams['font.size'] = 18
    mpl.rcParams['legend.fontsize'] = 18
    f, axarr = plt.subplots(2,2)
    axarr = axarr.flatten()

    xticks = range(0,int(nyears*12+1),60)
    xticklabels = [int(period[:4])+x/12 for x in xticks]
    xlabel = 'time (years)'


    for i,bid in enumerate([0,1,3,2]):#range(len(area)):
        plot_model_vs_mar(axarr[i],bid)
        axarr[i].text(-0.2,1.1,"(%s)"%labels[i],fontsize=20,transform=axarr[i].transAxes)

    plt.tight_layout()
    f.savefig('reports/figures/optimization_cumsum_basins_%s.pdf'%period, type='PDF',bbox_inches='tight', pad_inches = 0.01)
    print 'Total Ice Sheet SMB: SEMIC %2.0f, MAR %2.0f, SEMIC-MAR: %2.0f (%3.1f%%)' % (total_ice_semic[0,-1],
                                                                             total_ice_mar[0,-1],
                                                                             total_ice_semic[0,-1]-total_ice_mar[0,-1],
                                                                             (total_ice_semic[0,-1]-total_ice_mar[0,-1])/total_ice_semic[0,-1]*100.)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

def get_Paired():
    _Paired_data = {
     'blue': [(0.0, 0.89019608497619629,
     0.89019608497619629), (0.090909090909090912, 0.70588237047195435,
     0.70588237047195435), (0.18181818181818182, 0.54117649793624878,
     0.54117649793624878), (0.27272727272727271, 0.17254902422428131,
     0.17254902422428131), (0.36363636363636365, 0.60000002384185791,
     0.60000002384185791), (0.45454545454545453, 0.10980392247438431,
     0.10980392247438431), (0.54545454545454541, 0.43529412150382996,
     0.43529412150382996), (0.63636363636363635, 0.0, 0.0),
     (0.72727272727272729, 0.83921569585800171, 0.83921569585800171),
     (0.81818181818181823, 0.60392159223556519, 0.60392159223556519),
     (0.90909090909090906, 0.60000002384185791, 0.60000002384185791), (1.0,
     0.15686275064945221, 0.15686275064945221)],

     'green': [(0.0, 0.80784314870834351, 0.80784314870834351),
     (0.090909090909090912, 0.47058823704719543, 0.47058823704719543),
     (0.18181818181818182, 0.87450981140136719, 0.87450981140136719),
     (0.27272727272727271, 0.62745100259780884, 0.62745100259780884),
     (0.36363636363636365, 0.60392159223556519, 0.60392159223556519),
     (0.45454545454545453, 0.10196078568696976, 0.10196078568696976),
     (0.54545454545454541, 0.74901962280273438, 0.74901962280273438),
     (0.63636363636363635, 0.49803921580314636, 0.49803921580314636),
     (0.72727272727272729, 0.69803923368453979, 0.69803923368453979),
     (0.81818181818181823, 0.23921568691730499, 0.23921568691730499),
     (0.90909090909090906, 1.0, 1.0), (1.0, 0.3490196168422699,
     0.3490196168422699)],

     'red': [(0.0, 0.65098041296005249, 0.65098041296005249),
     (0.090909090909090912, 0.12156862765550613, 0.12156862765550613),
     (0.18181818181818182, 0.69803923368453979, 0.69803923368453979),
     (0.27272727272727271, 0.20000000298023224, 0.20000000298023224),
     (0.36363636363636365, 0.9843137264251709, 0.9843137264251709),
     (0.45454545454545453, 0.89019608497619629, 0.89019608497619629),
     (0.54545454545454541, 0.99215686321258545, 0.99215686321258545),
     (0.63636363636363635, 1.0, 1.0), (0.72727272727272729,
     0.7921568751335144, 0.7921568751335144), (0.81818181818181823,
     0.41568627953529358, 0.41568627953529358), (0.90909090909090906,
     1.0, 1.0), (1.0, 0.69411766529083252, 0.69411766529083252)]}
    return mpl.cm.colors.LinearSegmentedColormap('Paired',_Paired_data)

def draw_annual_mean_maps(period,figname):
    print "====================="
    print "Draw annual mean maps"
    print "====================="

    fnm_semic = './data/processed/semic_%s_opti_d_am.nc' % period
    fnm_mar   = './data/interim/forcing_%s_am.nc' % period

    mpl.rcParams['font.size'] = 16

    nc1 = nc4.Dataset(fnm_mar,'r')
    mask = np.array(nc1.variables['SOL'][:]).squeeze()
    basins = np.array(nc1.variables['MSK'][:]).squeeze()
    lon, lat = get_lonlat(nc1)
    nc1.close()

    lsm = mask.copy()

    mask[mask<=10.5] = np.nan


    def show_plot(fnm1,fnm2,labels,t=(None,None,None,None),sm=(None,None,None,None),ac=(None,None,None,None),ab=(None,None,None,None),\
                  al=(None,None,None,None),lh=(None,None,None,None),sh=(None,None,None,None),stitle=None,addT=True):
        
        nc2 = nc4.Dataset(fnm1,'r')
        tsurf, smb, acc, abl, swnet, swnet_mar = read_data(nc2,'tsurf','smb','acc','melt','swnet','swnet_mar')
        tsurf = np.array(tsurf).squeeze()
        smb *= 86.4e6
        acc *= 86.4e6
        abl *= 86.4e6

        nc3 = nc4.Dataset(fnm2,'r')
        tsurf_mar, smb_mar, abl_mar, sf, alb_mar, swd, rz = read_data(nc3,'STT','SMB','ME','SF','AL1','SWD','RZ')
        acc_mar = smb_mar + abl_mar - rz
        f = plt.figure(figsize=(12,12))
        m = GreenlandMap(0,0)
        x, y = m(lon,lat)


        cmap = get_Paired()
        def individual_plot(i0,var1,var2,yname,vmin,vmax,dmin,dmax,unit):
            nx = 4
            plt.subplot(3,nx,i0)
            data = np.ma.masked_array(var1,mask=np.isnan(mask))
            im = m.pcolormesh(x,y,data,cmap=cmap,vmin=vmin,vmax=vmax,rasterized=True)
            m.colorbar(im)
            co = m.contour(x,y,basins,np.arange(2.5,5.5,1.0),colors='k',alpha=0.5)
            m.drawcoastlines().set_alpha(0.25)
            plt.title(yname+' '+unit)
            plt.ylabel('SEMIC')
            plt.text(0.02,0.02,labels[i0-1],fontsize=20,transform=plt.gca().transAxes)

            plt.subplot(3,nx,i0+nx)
            data = np.ma.masked_array(var2,mask=np.isnan(mask))
            print yname, np.min(data), np.max(data)
            im = m.pcolormesh(x,y,data,cmap=cmap,vmin=vmin,vmax=vmax,rasterized=True)
            m.colorbar(im)
            co = m.contour(x,y,basins,np.arange(2.5,5.5,1.0),colors='k',alpha=0.5)
            m.drawcoastlines().set_alpha(0.25)
            plt.ylabel('MAR/CanESM2')
            plt.text(0.02,0.02,labels[i0+nx-1],fontsize=20,transform=plt.gca().transAxes)

            plt.subplot(3,nx,i0+2*nx)
            data = np.ma.masked_array(var1-var2,mask=np.isnan(mask))
            im = m.pcolormesh(x,y,data,cmap='BrBG_r',vmin=dmin,vmax=dmax,rasterized=True)
            m.colorbar(im)
            co = m.contour(x,y,basins,np.arange(2.5,5.5,1.0),colors='k',alpha=0.5)
            m.drawcoastlines().set_alpha(0.25)
            plt.ylabel('SEMIC - MAR/CanESM2')
            plt.text(0.02,0.02,labels[i0+2*nx-1],fontsize=20,transform=plt.gca().transAxes)

        constT = 0.0
        if addT: constT = 273.15
        individual_plot(1,tsurf,tsurf_mar+constT,r'$T_s$',t[0],t[1],t[2],t[3],'[K]')
        individual_plot(2,swnet,swnet_mar,r'$SW_\mathrm{net}$',25,90,-10,10,r'[W m$^{-2}$]')
        individual_plot(3,smb,smb_mar,r'$SMB$',sm[0],sm[1],sm[2],sm[3],r'[mm day$^{-1}$]')
        individual_plot(4,abl,abl_mar,r'$M$',ab[0],ab[1],ab[2],ab[3],r'[mm day$^{-1}$]')
        plt.suptitle(stitle,y=1.02,fontsize=18)

        plt.tight_layout()
        return f


    labels = ['(%s)'%c for c in 'abcdefghijkl']
    f = show_plot(fnm_semic,fnm_mar,labels,\
                  t=(240,270,-1.5,1.5),sm=(-10,5,-1.5,1.5),ac=(0,6,-1.5,1.5),ab=(0,10,-1.5,1.5),stitle='Multi-year Mean (%s)'%period)

    f.savefig('reports/figures/%s'%figname,type='PDF', bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

def draw_simple_parameter_sensitivity():
    print "================================="
    print "Draw simple parameter sensitivity"
    print "================================="

    def read_nml(fnm,sect=None):
        f=open(fnm,'r')
        nml = {}
        app = True
        for line in f:
            if sect is not None:
                if sect in line:
                    app = True
                    continue
                if '/' in line:
                    app = False
            if app:
                x = line.split()
                if len(x) > 1:
                    k = x[0]
                    v = x[2][:-1]
                    try:
                        nml[k] = float(v)
                    except:
                        nml[k] = v
        f.close()
        return nml

    mpl.rcParams['figure.figsize'] = 8,8
    mpl.rcParams['font.size'] = 18

    opti_nml = read_nml('config/surface_physics.nml',sect='surface_physics')

    def read_files(par_name):
        path = 'data/raw/sensitivity/'
        # non-optimal (initial distribution of parameters)
        files = glob.glob(path+'semic_'+par_name+'_??????.out')
        data = []
        for f in files:
            data.append(np.loadtxt(f))
        data = np.array(data).squeeze()

        files = glob.glob(path+'semic_'+par_name+'_??????.nml')
        data_nml = []
        for f in files:
            data_nml.append(read_nml(f,sect='surface_physics'))
        return data, data_nml

    def plot_cost(ax,pname,names,plims):
        data, data_nml = read_files(pname)
        y_tot = []
        x = []
        # loop over parameter sensitivity files
        for k,y in enumerate(data):
            if np.isfinite(y):
                y_tot.append(y)
                x.append(data_nml[k][pname])
        l, = ax.plot(x,y_tot,lw=3,c='k')
        idx = (np.abs(np.array(y_tot)-opti_nml[pname])).argmin()
        ax.plot(opti_nml[pname],y_tot[np.argmin(y_tot)],'ro')
        print pname, x[np.argmin(y_tot)], y_tot[np.argmin(y_tot)]
        ax.set_xlabel(names,fontsize=18)
        ax.set_xlim(plims[0],plims[1])
        ax.set_ylim(0.2,0.5)
        start, end = ax.get_xlim()
        tkw = dict(size=4, width=1.5)
        ax.grid()
        return x

    params= ['amp', 'alb_smax', 'hcrit', 'albi', 'rcrit', 'albl']
    param_names = [r'$A$ (in K)', r'$\alpha_s$',
                   r'$h_\mathsf{crit}$ (in m)', r'$\alpha_i$',
                   r'$f_R$', r'$\alpha_l$'
                   ]

    fnm = './config/namelist.ranges'
    plims = {}
    with open (fnm,'r') as f:
        for line in f.readlines():
            plims[line.split(',')[0]] = [float(x) for x in line.rstrip().split(',')[1:]]
    #plims = [[0,5,1],[0.7,0.95,0.05],[0,0.2,0.05],[0.25,0.55,0.05],[0.0,1.0,0.2],[0.05,0.35,0.05]]
    f, axarr = plt.subplots(3,2)

    labels = "abcdef"
    p = 0
    for i in range(3):
        for j in range(2):
            if p < len(params):
                print plims[params[p]]
                x = plot_cost(axarr[i,j],params[p],param_names[p],plims[params[p]])
                if j==0: axarr[i,j].set_ylabel('cost function')
                #if p > 1: axarr[i,j].set_ylim(0.22,0.28)
                axarr[i,j].text(-0.2,1.15,"(%s)"%labels[p],fontsize=20,transform=axarr[i,j].transAxes)
            p += 1

    plt.tight_layout()
    f.savefig('reports/figures/fig08.pdf',\
             type='PDF',bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

def draw_parameter_sensitivity():
    print "=========================="
    print "Draw parameter sensitivity"
    print "=========================="

    xsize = [851., 405., 1160., 1320., 851., 405., 1160., 1320.]
    totsize = sum(xsize)
    xfrac = [x/totsize for x in xsize]

    def crmse(x1,x2,scaled=True):
        nx = len(x1)

        x1_avg = np.sum(x1)/nx
        x2_avg = np.sum(x2)/nx
        x2_std = np.sqrt(np.sum((x2 - x2_avg)**2)/nx)
        x1_std = np.sqrt(np.sum((x1 - x1_avg)**2)/nx)
        if (scaled):
            y = np.sqrt(np.sum(((x1 - x1_avg)-(x2 - x2_avg))**2)/nx)/x2_std
            res = np.sqrt(y**2 + (x1_std/x2_std - 1.0)**2)
        else:
            res = np.sqrt(np.sum(((x1/x1_avg - 1.0)-(x2/x2_avg - 1.0))**2)/nx + (x1_std - x2_std)**2)
        return res

    def read_nml(fnm,sect=None):
        f=open(fnm,'r')
        nml = {}
        app = True
        for line in f:
            if sect is not None:
                if sect in line:
                    app = True
                    continue
                if '/' in line:
                    app = False
            if app:
                x = line.split()
                if len(x) > 1:
                    k = x[0]
                    v = x[2][:-1]
                    try:
                        nml[k] = float(v)
                    except:
                        nml[k] = v
        f.close()
        return nml

    mpl.rcParams['figure.figsize'] = 9,6
    mpl.rcParams['font.size'] = 14

    var_names = ['Surface Temperature (K)','Net Shortwave (W/m2)',\
                 'Surface Mass Balance (mm/day)','Ablation (mm/day)']

    def read_files(par_name,load=False):
        path = 'data/raw/sensitivity/'
        obj_file = path+'semic_'+par_name+'.obj'
        if load:
            f = open(obj_file,'r')
            data, data_nml = pickle.load(f)
            f.close()
        else:
            # non-optimal (initial distribution of parameters)
            files = glob.glob(path+'semic_'+par_name+'_??????.nc')
            data = []
            for f in files:
                nc = nc4.Dataset(f,'r')
                data.append(read_data(nc,'tsurf_ts','swnet_ts','smb_ts','melt_ts'))
            data = np.array(data).squeeze()
            print data.shape

            files = glob.glob(path+'semic_'+par_name+'_??????.nml')
            data_nml = []
            for f in files:
                data_nml.append(read_nml(f,sect='surface_physics'))
            f = open(obj_file,'w')
            pickle.dump((data,data_nml),f)
            f.close()
        return data, data_nml

    # optimal files
    nc = nc4.Dataset('data/raw/semic_1990-2000_opti_ts.nc','r')
    opti_1 = np.array(read_data(nc,'tsurf_ts','swnet_ts','smb_ts','melt_ts')).squeeze()
    # original
    orig_1 = np.array(read_data(nc,'tsurf_mar_ts','swnet_mar_ts','smb_mar_ts','melt_mar_ts')).squeeze()
    nc.close()
    nc = nc4.Dataset('data/raw/semic_2090-2100_opti_ts.nc','r')
    opti_2 = np.array(read_data(nc,'tsurf_ts','swnet_ts','smb_ts','melt_ts')).squeeze()
    # original
    orig_2 = np.array(read_data(nc,'tsurf_mar_ts','swnet_mar_ts','smb_mar_ts','melt_mar_ts')).squeeze()
    nc.close()

    orig = np.vstack([orig_1.T,orig_2.T]).T
    opti = np.vstack([opti_1.T,opti_2.T]).T
    opti_nml = read_nml('config/surface_physics.nml',sect='surface_physics')

    nbasin = orig.shape[0]

    print opti.shape, orig.shape

    colors = ['black','dodgerblue','forestgreen','red','purple']

    def plot_cost(ax,pname,names,plims):
        data, data_nml = read_files(pname,load=False)
        y_smb = []
        y_swnet = []
        y_melt = []
        y_tsurf = []
        y_tot = []
        x = []
        # loop over parameter sensitivity files
        for k in range(len(data[:,0,0,0])):
            cost_tsurf = np.sqrt(np.sum([(crmse(orig[0,:,i],data[k,0,:,i])*xfrac[i])**2 for i in range(nbasin)]))
            cost_swnet = np.sqrt(np.sum([(crmse(orig[1,:,i],data[k,1,:,i])*xfrac[i])**2 for i in range(nbasin)]))
            #cost_smb   = np.sqrt(np.sum([(crmse(np.cumsum(xsize[i]*orig[2,:,i]),
            #                                   np.cumsum(xsize[i]*data[k,2,:,i]))*xfrac[i])**2 for i in range(4)]))
            #cost_melt  = np.sqrt(np.sum([(crmse(np.cumsum(xsize[i]*orig[3,:,i]),
            #                                   np.cumsum(xsize[i]*data[k,3,:,i]))*xfrac[i])**2 for i in range(4)]))
            cost_smb   = np.sqrt(np.sum([(crmse(orig[2,:,i],data[k,3,:,i])*xfrac[i])**2 for i in range(nbasin)]))
            cost_melt  = np.sqrt(np.sum([(crmse(orig[3,:,i],data[k,3,:,i])*xfrac[i])**2 for i in range(nbasin)]))
            cost_tot   = np.sqrt(cost_tsurf**2 + cost_swnet**2 + cost_smb**2 + cost_melt**2)
            y_smb.append(cost_smb)
            y_swnet.append(cost_swnet)
            y_melt.append(cost_melt)
            y_tsurf.append(cost_tsurf)
            y_tot.append(cost_tot)
            x.append(data_nml[k][pname])
        # sort both lists in ascending x-order
        x, y_tot, y_smb, y_swnet, y_melt, y_tsurf = zip(*sorted(zip(x,y_tot,y_smb,y_swnet,y_melt,y_tsurf)))
        ys = [y_tot, y_smb, y_swnet, y_melt, y_tsurf]
        labels = ['total', r'SMB', r'SW$_{net}$', r'Melt', r'T$_s$']
        for i,y in enumerate(ys):
            # plot cost functions
            l, = ax.plot(x,y,lw=3,c=colors[i],label=labels[i])
            # indicate cost-function minimum
            idx = np.argmin(y)
        ax.axvline(opti_nml[pname],color='k',ls='--')
        print pname, x[np.argmin(y_tot)], y_tot[np.argmin(y_tot)]
        ax.set_xlabel(names,fontsize=18)
        ax.set_xlim(plims[0],plims[1])
        #ax.set_ylim(0,1.25)
        #ax.set_yticks(np.arange(0.0,1.26,0.25))
        ax.set_yscale('log')
        if pname in ['amp','alb_smin']: ax.set_ylabel(r'cost function',fontsize=18)
        start, end = ax.get_xlim()
        ax.xaxis.set_ticks(np.arange(start, end*1.01, plims[2]))
        tkw = dict(size=4, width=1.5)
        print pname, cost_tot, cost_swnet, cost_smb, cost_melt, cost_tsurf
        ax.grid()
        return x

    params= ['amp', 'alb_smax', 'albi', 'albl', 'hcrit', 'rcrit']
    param_names = [r'$A$ (in K)', r'$\alpha_s$', r'$\alpha_i$', r'$\alpha_l$', \
                   r'$h_\mathrm{\mathsf{crit}}$ (in m)', r'$f_R$']

    plims = [[0,5,1],[0.7,0.95,0.05],[0.25,0.55,0.05],[0.05,0.30,0.05],[0,0.5,0.1],[0.0,1.0,0.2]]
    f, axarr = plt.subplots(3,2)

    p = 0
    for i in range(3):
        for j in range(2):
            if p < len(params):
                x = plot_cost(axarr[i,j],params[p],param_names[p],plims[p])
            p += 1

    plt.legend(bbox_to_anchor=(1.5, 1),fontsize=14)
    plt.tight_layout()
    f.savefig('reports/figures/parameter_sensitivity.pdf',\
             type='PDF',bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

def draw_validation_time_series():
    print "==========================="
    print "Draw validation time series"
    print "==========================="

    mpl.rcParams['font.size'] = 20

    # basin areas; to convert from rates to annual accumulated values
    # for i in {1..8}; do echo -n "basin $i "; cdo -s -output -fldsum -eqc,$i -selname,basin ekholm_basins.nc; done
    # km^2
    area = [531875., 253125., 725000., 825000.]

    hist = 'data/processed/semic_historical_ts_ym.nc'
    nc1 = nc4.Dataset(hist,'r')
    t_hist_smb, t_hist_mar, a_hist_smb, a_hist_mar, \
    s_hist_smb, s_hist_mar, abl_hist_smb, abl_hist_mar = read_data(nc1,'tsurf_ts','tsurf_mar_ts',\
                                                                  'swnet_ts','swnet_mar_ts',\
                                                                  'smb_ts','smb_mar_ts',\
                                                                  'melt_ts','melt_mar_ts')
    hist_dtime = np.array(nc1.variables['time'][:]).squeeze()

    rcp85 = 'data/processed/semic_rcp85_ts_ym.nc'
    nc2 = nc4.Dataset(rcp85,'r')
    t_rcp85_smb, t_rcp85_mar, a_rcp85_smb, a_rcp85_mar, \
    s_rcp85_smb, s_rcp85_mar, abl_rcp85_smb, abl_rcp85_mar = read_data(nc2,'tsurf_ts','tsurf_mar_ts',\
                                                                       'swnet_ts','swnet_mar_ts',\
                                                                       'smb_ts','smb_mar_ts',\
                                                                       'melt_ts','melt_mar_ts')
    rcp85_dtime = np.array(nc2.variables['time'][:]).squeeze()
    nc2.close()

    colors = ['grey','red','dodgerblue','purple','orange','forestgreen','black','magenta','sandybrown']
    colors = ['goldenrod','red','forestgreen','dodgerblue','grey','purple','orange','magenta','sandybrown']

    def plot_data(ax,x1,x2,starty,dtime,unit,ymin,ymax,use_area=False,show_total=False):
        # baseline date from netCDF files
        baseyr = date(starty-1,6,30)
        # create date list
        date_list = [baseyr + timedelta(days=i) for i in dtime]
        # convert dates to integers
        fds = dates.date2num(date_list)
        # create date formatter
        hfmt = dates.DateFormatter('%Y')
        if use_area:
            a = np.array(area)*365.*1.e-6
        else:
            a = np.ones(4)
        if show_total:
            name = 'total ice'
            y1 = np.zeros(fds.shape)
            y2 = np.zeros(fds.shape)
            for i in range(1,4):
                y1 += x1[:,i]*a[i]
                y2 += x2[:,i]*a[i]
            line, = ax.plot(fds,y1,'k--',lw=3,label=name)
            color = line.get_color()
            ax.plot(fds,y2,'k--',lw=3,alpha=0.5)
        for i in range(4):#[0,1,3,5,6]:
            name = 'region %d' % (i)
            if i == 0: name = 'land'
            line, = ax.plot(fds,x1[:,i]*a[i],lw=3,color=colors[i],label=name,alpha=0.8)
            color = line.get_color()
            ax.plot(fds[:],x2[:,i]*a[i],lw=3,ls='--',color=color,alpha=0.8)

        ax.grid()
        ax.set_xlim(fds[0],fds[-1])
        ax.set_ylim(ymin,ymax)

        year = dates.YearLocator(10)
        ax.xaxis.set_major_locator(year)
        ax.xaxis.set_major_formatter(hfmt)
        ax.set_ylabel(unit)
        ax.set_xlabel('year')
        
    f = plt.figure(figsize=(14,14))
    ax1 = plt.subplot2grid((4,4), (0,0), colspan=1)
    ax2 = plt.subplot2grid((4,4), (0,1), colspan=3)
    ax3 = plt.subplot2grid((4,4), (1,0), colspan=1)
    ax4 = plt.subplot2grid((4,4), (1,1), colspan=3)
    ax5 = plt.subplot2grid((4,4), (2,0), colspan=1)
    ax6 = plt.subplot2grid((4,4), (2,1), colspan=3)

    tlist = [t_hist_mar,t_hist_smb,t_rcp85_mar,t_rcp85_smb]
    slist = [s_hist_mar,s_hist_smb,s_rcp85_mar,s_rcp85_smb]
    abllist = [abl_hist_mar,abl_hist_smb,abl_rcp85_mar,abl_rcp85_smb]

    tmin, tmax = 240, 270
    smin, smax = -900,450
    ablmin, ablmax = 0,1300

    plot_data(ax1,t_hist_smb,t_hist_mar,1970,hist_dtime,'$T_s$ [K]',tmin,tmax)
    plot_data(ax3,s_hist_smb*86.4e6,s_hist_mar*86.4e6,1970,hist_dtime,\
              r'$SMB$ [Gt a$^{-1}$]',smin,smax,use_area=True)
    plot_data(ax5,abl_hist_smb*86.4e6,abl_hist_mar*86.4e6,1970,hist_dtime,\
              r'$M$ [Gt a$^{-1}$]',ablmin,ablmax,use_area=True)
    ax1.set_xlabel('')
    ax3.set_xlabel('')

    ax1.axes.xaxis.set_ticklabels([])
    ax3.axes.xaxis.set_ticklabels([])
    ax1.set_title('historical')

    plot_data(ax2,t_rcp85_smb,t_rcp85_mar,2006,rcp85_dtime,'',tmin,tmax)

    ax2.set_xlabel('')
    ax2.axes.yaxis.set_ticklabels([])
    ax2.axes.xaxis.set_ticklabels([])
    ax2.set_title('RCP8.5')

    plot_data(ax4,s_rcp85_smb*86.4e6,s_rcp85_mar*86.4e6,2006,rcp85_dtime,'',smin,smax,use_area=True)
    plot_data(ax6,abl_rcp85_smb*86.4e6,abl_rcp85_mar*86.4e6,2006,rcp85_dtime,'',ablmin,ablmax,use_area=True)
    ax4.set_xlabel('')
    ax4.axes.yaxis.set_ticklabels([])
    ax4.axes.xaxis.set_ticklabels([])
    ax6.axes.yaxis.set_ticklabels([])

    tmp_ticks = [240,250,260,270]
    ax1.set_yticks(tmp_ticks)
    ax2.set_yticks(tmp_ticks)

    smb_ticks = [-800,-400,0,400]
    ax3.set_yticks(smb_ticks)
    ax4.set_yticks(smb_ticks)

    abl_ticks = [0,400,800,1200]
    ax5.set_yticks(abl_ticks)
    ax6.set_yticks(abl_ticks)

    ax4.legend(bbox_to_anchor=(-0.3, -0.2, 0.8, .102), ncol=7, mode="expand", borderaxespad=0.,prop={'size':16})

    # custom legend for SEMIC and MAR line styles
    semic_line = mpl.lines.Line2D([], [], color='black', ls='-',  lw=3, alpha=0.5, label='SEMIC')
    mar_line   = mpl.lines.Line2D([], [], color='black', ls='--', lw=3, alpha=0.5, label='MAR')

    handles = [semic_line, mar_line]
    labels = [h.get_label() for h in handles]

    ax6.legend(bbox_to_anchor=(0.55, 1.2, 0.3, .102), handles=handles, labels=labels, ncol=2, mode="expand", borderaxespad=0. ,prop={'size':16})
    ax1.text(-0.5,1.0,"(a)",fontsize=30,transform=ax1.transAxes)
    ax3.text(-0.5,1.0,"(b)",fontsize=30,transform=ax3.transAxes)
    ax5.text(-0.5,1.0,"(c)",fontsize=30,transform=ax5.transAxes)

    plt.tight_layout()
    f.savefig('reports/figures/semic_validation_timeseries.pdf', type='PDF', bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

def draw_validation_comparison():
    print "=========================="
    print "Draw validation comparison"
    print "=========================="

    mpl.rcParams['font.size'] = 20

    # basin areas; to convert from rates to annual accumulated values
    # for i in {1..8}; do echo -n "basin $i "; cdo -s -output -fldsum -eqc,$i -selname,basin ekholm_basins.nc; done
    # km^2
    area = [531875., 253125., 725000., 825000.]

    hist = 'data/processed/semic_historical_ts_ym.nc'
    nc1 = nc4.Dataset(hist,'r')
    t_hist_smb, t_hist_mar, a_hist_smb, a_hist_mar, \
    s_hist_smb, s_hist_mar, abl_hist_smb, abl_hist_mar = read_data(nc1,'tsurf_ts','tsurf_mar_ts',\
                                                                  'swnet_ts','swnet_mar_ts',\
                                                                  'smb_ts','smb_mar_ts',\
                                                                  'melt_ts','melt_mar_ts')
    hist_dtime = np.array(nc1.variables['time'][:]).squeeze()
    nc1.close()

    rcp85 = 'data/processed/semic_rcp85_ts_ym.nc'
    nc2 = nc4.Dataset(rcp85,'r')
    t_rcp85_smb, t_rcp85_mar, a_rcp85_smb, a_rcp85_mar, \
    s_rcp85_smb, s_rcp85_mar, abl_rcp85_smb, abl_rcp85_mar = read_data(nc2,'tsurf_ts','tsurf_mar_ts',\
                                                                       'swnet_ts','swnet_mar_ts',\
                                                                       'smb_ts','smb_mar_ts',\
                                                                       'melt_ts','melt_mar_ts')
    rcp85_dtime = np.array(nc2.variables['time'][:]).squeeze()
    nc2.close()

    colors = ['goldenrod','red','forestgreen','dodgerblue','grey','purple','orange','magenta','sandybrown']

    def plot_data(ax,x1,x2,starty,dtime,unit,ymin,ymax,use_area=False,show_total=False):
        # baseline date from netCDF files
        baseyr = date(starty,1,1)
        # create date list
        date_list = [baseyr + timedelta(days=i) for i in dtime]
        # convert dates to integers
        fds = dates.date2num(date_list)
        # create date formatter
        hfmt = dates.DateFormatter('%Y')
        if use_area:
            a = np.array(area)*365.*1.e-6
        else:
            a = np.ones(4)
            
        ax.plot(np.arange(ymin,2*ymax),np.arange(ymin,2*ymax),'k-')
        if show_total:
            name = 'total ice'
            y1 = np.zeros(fds.shape)
            y2 = np.zeros(fds.shape)
            for i in range(1,4):
                y1 += x1[:,i]*a[i]
                y2 += x2[:,i]*a[i]
            line, = ax.plot(y1,y2,ls='',marker='o',color='k',label=name,alpha=0.75,mew=0.0)
        for i in range(4):
            name = 'basin %d' % i
            if i == 0: name = 'land'

            line, = ax.plot(x1[:,i]*a[i],x2[:,i]*a[i],ls='',marker='o',color=colors[i],label=name,alpha=0.75,mew=0.0)
        
        ax.grid()
        ax.set_ylabel('MAR/CanESM2')
        ax.set_xlabel('SEMIC')
        ax.set_xlim(ymin,ymax)
        ax.set_ylim(ymin,ymax)
        
    f = plt.figure(figsize=(9,14))
    ax1 = plt.subplot2grid((4,2), (0,0), colspan=1)
    ax2 = plt.subplot2grid((4,2), (0,1), colspan=1)
    ax3 = plt.subplot2grid((4,2), (1,0), colspan=1)
    ax4 = plt.subplot2grid((4,2), (1,1), colspan=1)
    ax5 = plt.subplot2grid((4,2), (2,0), colspan=1)
    ax6 = plt.subplot2grid((4,2), (2,1), colspan=1)


    tlist = [t_hist_mar,t_hist_smb,t_rcp85_mar,t_rcp85_smb]
    slist = [s_hist_mar,s_hist_smb,s_rcp85_mar,s_rcp85_smb]
    abllist = [abl_hist_mar,abl_hist_smb,abl_rcp85_mar,abl_rcp85_smb]

    tmin, tmax = 240, 270
    smin, smax = -900,450
    ablmin, ablmax = 0,1300
    accmin, accmax = 0,300

    plot_data(ax1,t_hist_smb,t_hist_mar,1970,hist_dtime,'temperature',tmin,tmax)
    plot_data(ax3,s_hist_smb*86.4e6,s_hist_mar*86.4e6,1970,hist_dtime,\
              'mass balance',smin,smax,use_area=True)
    plot_data(ax5,abl_hist_smb*86.4e6,abl_hist_mar*86.4e6,1970,hist_dtime,\
              'surface melt',ablmin,ablmax,use_area=True)
    ax1.set_title('historical')

    plot_data(ax2,t_rcp85_smb,t_rcp85_mar,2006,rcp85_dtime,'temperature',tmin,tmax)

    ax2.set_title('RCP8.5')

    plot_data(ax4,s_rcp85_smb*86.4e6,s_rcp85_mar*86.4e6,2006,rcp85_dtime,'mass balance',smin,smax,use_area=True)
    plot_data(ax6,abl_rcp85_smb*86.4e6,abl_rcp85_mar*86.4e6,2006,rcp85_dtime,'surface melt',ablmin,ablmax,use_area=True)

    ax1.set_xlabel('')
    ax2.set_xlabel('')
    ax3.set_xlabel('')
    ax4.set_xlabel('')
    ax2.set_ylabel('')
    ax4.set_ylabel('')

    tmp_ticks = [240,250,260,270]
    ax1.set_xticks(tmp_ticks)
    ax1.set_yticks(tmp_ticks)
    ax2.set_xticks(tmp_ticks)
    ax2.set_yticks(tmp_ticks)

    smb_ticks = [-800,-400,0,400]
    ax3.set_xticks(smb_ticks)
    ax3.set_yticks(smb_ticks)
    ax4.set_xticks(smb_ticks)
    ax4.set_yticks(smb_ticks)

    abl_ticks = [0,400,800,1200]
    ax5.set_xticks(abl_ticks)
    ax5.set_yticks(abl_ticks)
    ax6.set_xticks(abl_ticks)
    ax6.set_yticks(abl_ticks)

    ax1.text(-0.5,1.0,"(d)",fontsize=30,transform=ax1.transAxes)
    ax3.text(-0.5,1.0,"(e)",fontsize=30,transform=ax3.transAxes)
    ax5.text(-0.5,1.0,"(f)",fontsize=30,transform=ax5.transAxes)

    plt.tight_layout()
    f.savefig('reports/figures/semic_validation_1to1.pdf',type='PDF', bbox_inches='tight', pad_inches = 0.01)
    print "Finished!"
    print "========="
    print ""
    print ""
    print ""

nyears = 10
draw_diurnal_cycle()#                                         Fig. 1
draw_region_mask()#                                           Fig. 2
draw_annual_mean_maps('1990-1999','fig03.pdf')#               Fig. 3
draw_annual_mean_maps('2090-2099','fig04.pdf')#               Fig. 4
draw_basin_time_series('1990-1999','fig05.pdf')#              Fig. 5
draw_basin_time_series('2090-2099','fig06.pdf')#              Fig. 6
draw_cumulative_time_series('1990-1999',"abcd",legend=False)# Fig. 7/1
draw_cumulative_time_series('2090-2099',"efgh")#              Fig. 7/2
draw_simple_parameter_sensitivity()#                          Fig. 8
draw_taylor('1990-1999',label="(a)",legend=False)#            Fig. 9/1
draw_taylor('2090-2099',label="(b)")#                         Fig. 9/2
draw_validation_time_series()#                                Fig. 10/1
draw_validation_comparison()#                                 Fig. 10/2
