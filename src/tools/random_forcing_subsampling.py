import numpy as np
import random
import netCDF4 as nc4
import sys

params1 = ["MSK","SOL"]
params2 = ["SF","RF","SP","LWD","SWD","STT","ME","RZ","SMB","AL1","SHF","LHF","SHSN3","TTMIN","TTMAX"]
params3 = ["UU","VV","TT","QQ"]

p_fettweis = "data/external/fettweis/MARv2/"

f_histo    = lambda y: p_fettweis + "MARv2-CanESM2-histo/MARv2-CanESM2-histo-%d.nc" % y
f_rcp85    = lambda y: p_fettweis + "MARv2-CanESM2-rcp85/MARv2-CanESM2-rcp85-%d.nc" % y

years = range(1970,2101)
years = range(2090,2101)
#years = range(2002,2012) # for testing

# periods need to have equal lengths
periods = [range(1990,2000), range(2090,2100)]
years = [item for sublist in periods for item in sublist]

d = {p: [] for p in params1+params2+params3}

# Get random sub-sampling mask for each basin
# 1) get basin mask
nc = nc4.Dataset(f_histo(1970),'r')
msk = np.array(nc.variables["MSK"][:]).flatten()
nc.close()

# 2) return random number array between 1 and n (-> 1/n of total values expected)
rand = lambda n: np.random.random(len(msk))*n

# 3) iterate over basins to mask random basin values (set to 1.0)
n = int(sys.argv[1])
ntotal = int(sum(np.where(msk>1,1.,0.))/n)
print 'Change in surface_driver.f90:  \" %s = %d\"' % ("dom%grid%G%nx",2*ntotal)
basins = {b-1: np.where(np.where(msk==b,rand(n)<1,False),1.,0.) for b in list(set(msk)) if b>1}
while (int(sum(sum(basins.values()))) != ntotal):
#    print int(sum(sum(basins.values()))), ntotal
    basins = {b-1: np.where(np.where(msk==b,rand(n)<1,False),1.,0.) for b in list(set(msk)) if b>1}
print {k: v.sum() for k,v in basins.iteritems()}

# 4) sum all values equal to 1.0 (should be a complementary set)
sample_mask = sum(basins.values())
print "Is sample mask complementary?", set(sample_mask) == set([0.0, 1.0])
print len(sample_mask), sum(sample_mask)

n_samples = int(sum(sample_mask))

# iterate over historical and rcp85 data set
for y in years:
    if y < 2006:
        fnm = f_histo(y)
    else:
        fnm = f_rcp85(y)
    print y, fnm
    nc = nc4.Dataset(fnm,'r')
    for k,_ in nc.variables.iteritems():
        if k in params1:
            d[k].append(np.array(nc.variables[k][:]).flatten()[sample_mask==1.0])
        if k in params2:
            d[k].append( np.array(nc.variables[k][:]).reshape(365,len(sample_mask))[365*[sample_mask==1.0]].reshape((365,n_samples)))
        if k in params3:
            d[k].append(np.array(nc.variables[k][:])[:,0,:,:].reshape(365,len(sample_mask))[365*[sample_mask==1.0]].reshape((365,n_samples)))
    nc.close()


# Write to NetCDF file
nperiods = len(periods)
nyears = len(periods[0])
ntime = nyears*365
fnm_out = sys.argv[2]
bmax = np.max(msk)-1

nc = nc4.Dataset(fnm_out,'w')

nc.createDimension('TIME', None)
time = nc.createVariable('TIME', 'i', ('TIME',))
time[:] = time
time.units =  'days since 1970-01-15 00:00:00'

nc.createDimension('X', n_samples*nperiods)
x = nc.createVariable('X', 'i', ('X',))
x.long_name = 'x-dimension'
x[:] = range(n_samples*nperiods)

nc.createDimension('Y', 1)
y = nc.createVariable('Y', 'i', ('Y',))
y.long_name = 'y-dimension'
y[:] = 1

# 'MSK'
x = nc.createVariable('MSK','f',('Y','X',))
x[:] = np.array([d['MSK'][0]+bmax*i for i in range(nperiods)]).reshape((1,n_samples*nperiods))
#x[:] = np.array([i for i in range(n_samples*nperiods)]).reshape((1,n_samples*nperiods))
# 'SOL'
x = nc.createVariable('SOL','f',('Y','X',))
x[:] = np.array([d['SOL'][0] for i in range(nperiods)]).reshape((1,n_samples*nperiods))
for p in params2+params3:
    x = nc.createVariable(p,'f',('TIME','X',))
    data = np.zeros((nyears*365,n_samples*nperiods))
    for i,years in enumerate(periods):
        start_p = i*n_samples
        for j,y in enumerate(years):
            start = j*365
            data[start:start+365,start_p:start_p+n_samples] = np.array(d[p][i+j])
    x[:] = data

nc.close()
