from tools import nml_to_dict, dict_to_nml, get_params
import numpy as np
from copy import deepcopy
import sys
import os.path
from subprocess import call

def write_namelist(fname,s):
    f=open(fname,'w')
    f.write(s)
    f.close()
    
max_iter = int(sys.argv[1])

nml_fnm = "./config/optimization.nml"
d = nml_to_dict(nml_fnm)
# append surface_physics namelist
d['surface_physics'] = nml_to_dict('./config/surface_physics.nml')['surface_physics']

params, names = get_params()
particle_space = {}
for p in params:
    particle_space[p[0]] = np.linspace(float(p[1]),float(p[2]),max_iter)

print particle_space

exe = 'bin/run_particles.x'
if not os.path.isfile(exe):
    print "\033[91mFile "+exe+" not found.\n Run:\033[0m make "+exe
    sys.exit()

for k,v in particle_space.iteritems():
    i = 0
    nml_prefix = "./data/raw/sensitivity/semic_%s_" %k
    for x in v:
        d_copy = deepcopy(d)
        d_copy['semic_output']['file_timser'] = '""'#./data/raw/sensitivity/semic_%s_%06d.nc"' % (k,i)
        d_copy['semic_output']['file_daily'] = '""'
        d_copy['surface_physics'][k] = str(x)
        fnm = nml_prefix+"%06d.nml" % i
        d_copy['semic_driver']['surface_physics_nml'] = '"%s"' % fnm
        s = dict_to_nml(d_copy)
        write_namelist(fnm,s)
        i += 1
    i0 = 0
    i1 = i-1
    call(["mpiexec","-n","2","./"+exe,nml_fnm,'"'+nml_prefix+'"',str(i0),str(i1)])


