!> ####################################################################
!! **Module**  : surface_driver_utilities \n
!! **Author**  : Mario Krapp \n 
!! **Purpose** : This module contains many helpful subroutines 
!!               to read input files 
!! ####################################################################
module surface_driver_utilities
  
    use ncio
    use interpolation

    implicit none
    double precision, parameter :: t0 = 273.15d0 !< freezing point of water
contains
      !> Read all necessary forcing fields from netCDF input file (MAR).
      subroutine read_forcing_mar(fnm_in,nx,ny,nt,uu,vv,tt,sf,rf,qq,sp,lwd,swd,amp)
  
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: ny !< y dimension
          integer, intent(in) :: nt !< time dimension
          character(len=256), intent(in) :: fnm_in !< MAR file
  
          double precision, allocatable, dimension(:,:,:,:) :: tmp1
          double precision, allocatable, dimension(:,:,:)   :: tmp2
          double precision, dimension(nx*ny,nt), intent(out) :: sf !< snow fall
          double precision, dimension(nx*ny,nt), intent(out) :: rf !< rain fall
          double precision, dimension(nx*ny,nt), intent(out) :: sp !< surface pressure
          double precision, dimension(nx*ny,nt), intent(out) :: lwd !< downward LW
          double precision, dimension(nx*ny,nt), intent(out) :: swd !< downward SW
          double precision, dimension(nx*ny,nt), intent(out) :: uu !< y-wind
          double precision, dimension(nx*ny,nt), intent(out) :: vv !< x-wind 
          double precision, dimension(nx*ny,nt), intent(out) :: tt !< air temperature
          double precision, dimension(nx*ny,nt), intent(out) :: qq !< specific humidity
          double precision, dimension(nx*ny,nt), intent(out) :: amp !< diurnal cycle amplitude (tmax-tmin)/2

          allocate(tmp1(nx,ny,1,nt))
          allocate(tmp2(nx,ny,nt))
  
          ! Read input data (forcing)
          call nc_read(fnm_in,"UU",tmp1)
          uu = reshape(tmp1,shape(uu))
          call nc_read(fnm_in,"VV",tmp1)
          vv = reshape(tmp1,shape(vv))
          call nc_read(fnm_in,"TT",tmp1)
          tt = reshape(tmp1,shape(tt))
          call nc_read(fnm_in,"QQ",tmp1)
          qq = reshape(tmp1,shape(qq))
          call nc_read(fnm_in,"SF",tmp2)
          sf = reshape(tmp2,shape(sf))
          call nc_read(fnm_in,"RF",tmp2)
          rf = reshape(tmp2,shape(rf))
          call nc_read(fnm_in,"SP",tmp2)
          sp = reshape(tmp2,shape(sp))
          call nc_read(fnm_in,"LWD",tmp2)
          lwd = reshape(tmp2,shape(lwd))
          call nc_read(fnm_in,"SWD",tmp2)
          swd = reshape(tmp2,shape(swd))
          call nc_read(fnm_in,"TTMAX",tmp1)
          amp = reshape(tmp1,shape(amp))
          call nc_read(fnm_in,"TTMIN",tmp1)
          amp = (amp - reshape(tmp1,shape(amp)))/2.0

          deallocate(tmp1)
          deallocate(tmp2)
  
      end subroutine read_forcing_mar
  

      !> Read all necessary validation fields from netCDF input file (MAR).
      subroutine read_validation_mar(fnm_in,nx,ny,nt,tsurf,alb,&
              melt,refr,massbal,shf,lhf,hsnow)
          ! Read in data from netCDF file which contains the fields
          ! to validate the model to. Conform output to 2 dimensions.
  
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: ny !< y dimension
          integer, intent(in) :: nt !< time dimension
          character(len=256), intent(in) :: fnm_in !< MAR file
          double precision, dimension(nx*ny,nt), intent(out) :: tsurf !< surface temperature
          double precision, dimension(nx*ny,nt), intent(out) :: alb !< surface albedo
          double precision, dimension(nx*ny,nt), intent(out) :: melt !< melt
          double precision, dimension(nx*ny,nt), intent(out) :: refr !< refreezing
          double precision, dimension(nx*ny,nt), intent(out) :: massbal !< mass balance
          double precision, dimension(nx*ny,nt), intent(out) :: shf !< sensible heat flux
          double precision, dimension(nx*ny,nt), intent(out) :: lhf !< latent heat flux
          double precision, dimension(nx*ny,nt), intent(out) :: hsnow !< snow height
  
          double precision, allocatable, dimension(:,:,:,:) :: tmp1
          double precision, allocatable, dimension(:,:,:)   :: tmp2

          allocate(tmp1(nx,ny,1,nt))
          allocate(tmp2(nx,ny,nt))
  
          ! Read arrays into validation fields
          tmp1 = -999.
          call nc_read(fnm_in,"STT",tmp1)
          tsurf = reshape(tmp1,shape(tsurf))
          tmp1 = -999.
          call nc_read(fnm_in,"ME",tmp1)
          melt = reshape(tmp1,shape(melt))
          tmp1 = -999.
          call nc_read(fnm_in,"RZ",tmp1)
          refr = reshape(tmp1,shape(refr))
          tmp1 = -999.
          call nc_read(fnm_in,"SMB",tmp1)
          massbal = reshape(tmp1,shape(massbal))
          tmp2 = -999.
          call nc_read(fnm_in,"AL1",tmp2)
          alb = reshape(tmp2,shape(alb))
          tmp2 = -999.
          call nc_read(fnm_in,"SHF",tmp2)
          shf = reshape(tmp2,shape(shf))
          tmp2 = -999.
          call nc_read(fnm_in,"LHF",tmp2)
          lhf = reshape(tmp2,shape(lhf))
          tmp2 = -999.
          call nc_read(fnm_in,"SHSN3",tmp1)
          hsnow = reshape(tmp1,shape(hsnow))

          deallocate(tmp1)
          deallocate(tmp2)
  
      end subroutine read_validation_mar
  
      !> Read all necessary forcing fields from netCDF input file (MAR sub-sampling).
      subroutine read_forcing_mar_sub_sampling(fnm_in,nx,nt,uu,vv,tt,sf,rf,qq,sp,lwd,swd,amp)
  
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: nt !< time dimension
          character(len=256), intent(in) :: fnm_in !< MAR file
  
          double precision, dimension(nx,nt), intent(out) :: sf !< snow fall
          double precision, dimension(nx,nt), intent(out) :: rf !< rain fall
          double precision, dimension(nx,nt), intent(out) :: sp !< surface pressure
          double precision, dimension(nx,nt), intent(out) :: lwd !< downward LW
          double precision, dimension(nx,nt), intent(out) :: swd !< downward SW
          double precision, dimension(nx,nt), intent(out) :: uu !< y-wind
          double precision, dimension(nx,nt), intent(out) :: vv !< x-wind 
          double precision, dimension(nx,nt), intent(out) :: tt !< air temperature
          double precision, dimension(nx,nt), intent(out) :: qq !< specific humidity
          double precision, dimension(nx,nt), intent(out) :: amp !< diurnal cycle amplitude (tmax-tmin)/2
          double precision, dimension(nx,nt)              :: ttmax !< ttmax
  
          ! Read input data (forcing)
          call nc_read(fnm_in,"UU",uu)
          call nc_read(fnm_in,"VV",vv)
          call nc_read(fnm_in,"TT",tt)
          call nc_read(fnm_in,"QQ",qq)
          call nc_read(fnm_in,"SF",sf)
          call nc_read(fnm_in,"RF",rf)
          call nc_read(fnm_in,"SP",sp)
          call nc_read(fnm_in,"LWD",lwd)
          call nc_read(fnm_in,"SWD",swd)
          call nc_read(fnm_in,"TTMAX",ttmax)
          call nc_read(fnm_in,"TTMIN",amp)
          amp = (ttmax-amp)/2.0

      end subroutine read_forcing_mar_sub_sampling

      !> Read all necessary validation fields from netCDF input file (MAR).
      subroutine read_validation_mar_sub_sampling(fnm_in,nx,nt,tsurf,alb,&
              melt,refr,massbal,shf,lhf,hsnow)
          ! Read in data from netCDF file which contains the fields
          ! to validate the model to. Conform output to 2 dimensions.
  
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: nt !< time dimension
          character(len=256), intent(in) :: fnm_in !< MAR file
          double precision, dimension(nx,nt), intent(out) :: tsurf !< surface temperature
          double precision, dimension(nx,nt), intent(out) :: alb !< surface albedo
          double precision, dimension(nx,nt), intent(out) :: melt !< melt
          double precision, dimension(nx,nt), intent(out) :: refr !< refreezing
          double precision, dimension(nx,nt), intent(out) :: massbal !< mass balance
          double precision, dimension(nx,nt), intent(out) :: shf !< sensible heat flux
          double precision, dimension(nx,nt), intent(out) :: lhf !< latent heat flux
          double precision, dimension(nx,nt), intent(out) :: hsnow !< snow height
  
          ! Read arrays into validation fields
          call nc_read(fnm_in,"STT",tsurf)
          call nc_read(fnm_in,"ME",melt)
          call nc_read(fnm_in,"RZ",refr)
          call nc_read(fnm_in,"SMB",massbal)
          call nc_read(fnm_in,"AL1",alb)
          call nc_read(fnm_in,"SHF",shf)
          call nc_read(fnm_in,"LHF",lhf)
          call nc_read(fnm_in,"SHSN3",hsnow)

      end subroutine read_validation_mar_sub_sampling
  
  
      !> Read all necessary forcing fields from netCDF input file (ASR).
      subroutine read_forcing_asr(fnm_in,nx,ny,nt,uu,vv,tt,sf,rf,qq,sp,lwd,swd)
  
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: ny !< y dimension
          integer, intent(in) :: nt !< time dimension
          character(len=256), intent(in) :: fnm_in !< ASR file
  
          double precision, dimension(nx*ny,nt), intent(out) :: sf !< snow fall
          double precision, dimension(nx*ny,nt), intent(out) :: rf !< rain fall
          double precision, dimension(nx*ny,nt), intent(out) :: sp !< surface pressure
          double precision, dimension(nx*ny,nt), intent(out) :: lwd !< downward LW
          double precision, dimension(nx*ny,nt), intent(out) :: swd !< downward SW
          double precision, dimension(nx*ny,nt), intent(out) :: uu !< y-wind
          double precision, dimension(nx*ny,nt), intent(out) :: vv !< x-wind 
          double precision, dimension(nx*ny,nt), intent(out) :: tt !< air temperature
          double precision, dimension(nx*ny,nt), intent(out) :: qq !< specific humidity
  
          double precision :: tmp(nx,ny,nt)
  
          ! Read input data (forcing)
          call nc_read(fnm_in,"U10E",tmp)
          uu = reshape(tmp,shape(uu))
          call nc_read(fnm_in,"V10E",tmp)
          vv = reshape(tmp,shape(vv))
          call nc_read(fnm_in,"T2M",tmp)
          tt = reshape(tmp,shape(tt))
          call nc_read(fnm_in,"Q2M",tmp)
          qq = reshape(tmp,shape(qq))
          call nc_read(fnm_in,"SR",tmp)
          sf = reshape(tmp,shape(sf))
          call nc_read(fnm_in,"RAINC",tmp)
          rf = reshape(tmp,shape(rf))
          call nc_read(fnm_in,"PSFC",tmp)
          sp = reshape(tmp,shape(sp))
          call nc_read(fnm_in,"LWDNB",tmp)
          lwd = reshape(tmp,shape(lwd))
          call nc_read(fnm_in,"SWDNB",tmp)
          swd = reshape(tmp,shape(swd))
  
      end subroutine read_forcing_asr
  
      !> Read all necessary forcing fields from netCDF input file (LOVECLIM).
      !!  If data are monthly mean data we need to interpolate:
      !!  1. Add last month before and first month after array
      !!  2. Interpolate along the time steps (surface_driver::interp)
      subroutine read_forcing_loveclim(fnm_in,nx,ny,ntin,ntout,wind,tt,sf,rf,qq,sp,lwd,swd)
  
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: ny !< y dimension
          integer, intent(in) :: ntin !< input time dimension
          integer, intent(in) :: ntout !< output time dimension
          character(len=256), intent(in) :: fnm_in !< LOVECLIM file
  
          double precision, dimension(nx*ny,ntout), intent(out) :: sf !< snow fall
          double precision, dimension(nx*ny,ntout), intent(out) :: rf !< rain fall
          double precision, dimension(nx*ny,ntout), intent(out) :: sp !< surface pressure
          double precision, dimension(nx*ny,ntout), intent(out) :: lwd !< downward LW
          double precision, dimension(nx*ny,ntout), intent(out) :: swd !< downward SW
          double precision, dimension(nx*ny,ntout), intent(out) :: wind !< wind speed
          double precision, dimension(nx*ny,ntout), intent(out) :: tt !< air temperature
          double precision, dimension(nx*ny,ntout), intent(out) :: qq !< specific humidity
  
          double precision :: tmp1(nx,ny,3,ntin), tmp2(nx,ny,ntin), tmp3(nx,ny,ntin+2), tmp_intp(nx,ny,ntout)
          double precision, dimension(nx*ny,ntout) :: tcc
          double precision, dimension(nx*ny,ntout) :: str, ssr, ts, alb, uu, vv
          double precision, dimension(:), allocatable :: xout, yout
          double precision, dimension(ntin+2) :: xi, yi
          double precision :: dx
          integer :: i, j
  
          double precision, parameter :: sigm = 5.67d-8
  
          dx = ntout/ntin
          allocate(xout(ntout+int(dx)-1))
          allocate(yout(ntout+int(dx)-1))
  
          if (ntin == ntout) then ! do nothing special
              ! Read input data (forcing)
              !call nc_read(fnm_in,"u",tmp1)
              !uu = reshape(tmp1(:,:,3,:),shape(uu))
              !call nc_read(fnm_in,"v",tmp1)
              !vv = reshape(tmp1(:,:,3,:),shape(vv))
              call nc_read(fnm_in,"wind",tmp2)
              wind = reshape(tmp2,shape(wind))
              call nc_read(fnm_in,"t2m",tmp2)
              tt = reshape(tmp2,shape(tt))
              call nc_read(fnm_in,"q",tmp2)
              qq = reshape(tmp2,shape(qq))
              call nc_read(fnm_in,"snow",tmp2)
              sf = reshape(tmp2,shape(sf))
              call nc_read(fnm_in,"pp",tmp2)
              rf = reshape(tmp2,shape(rf))
              call nc_read(fnm_in,"sp",tmp2)
              sp = reshape(tmp2,shape(sp))
              ! workaround because data only available as surface thermal/shortwave radiation
              ! lwd = str+lwu
              call nc_read(fnm_in,"str",tmp2)
              str = reshape(tmp2,shape(lwd))
              call nc_read(fnm_in,"ts",tmp2)
              lwd = str + (reshape(tmp2,shape(lwd))+t0)**4*sigm
              call nc_read(fnm_in,"ssr",tmp2)
              ssr = reshape(tmp2,shape(swd))
              call nc_read(fnm_in,"alb",tmp2)
              swd = ssr/(1.-reshape(tmp2,shape(swd)))
          else ! interpolate
              ! time axis
               do i=1,ntin+2
                   xi(i) = (i-1)*dx - dx/2.
               end do
              ! Read input data (forcing)
               call nc_read(fnm_in,"u",tmp1)
               call interp(tmp1(:,:,3,:),nx,ny,ntin,ntout,uu)
               tmp3(:,:,1) = tmp1(:,:,3,ntin)
               tmp3(:,:,2:ntin+1) = tmp1(:,:,3,:)
               tmp3(:,:,ntin+2) = tmp1(:,:,3,1)
               do i=1,nx
                   do j=1,ny
                       yi = tmp3(i,j,:)
                       call spline(ntin+1,ntout+int(dx),xi,yi,xout,yout)
                       tmp_intp(i,j,:) = yout(int(dx/2.)+1:ntout+int(dx))
                   end do
               end do
               uu = reshape(tmp_intp,shape(uu))
               call nc_read(fnm_in,"v",tmp1)
               call interp(tmp1(:,:,3,:),nx,ny,ntin,ntout,vv)
              !vv = reshape(tmp1(:,:,3,:),shape(vv))
              call nc_read(fnm_in,"wind",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,wind)
              call nc_read(fnm_in,"t2m",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,tt)
              !tt = reshape(tmp2,shape(tt))
              call nc_read(fnm_in,"r",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,qq)
              !qq = reshape(tmp2,shape(qq))
              call nc_read(fnm_in,"snow",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,sf)
              !sf = reshape(tmp2,shape(sf))
              call nc_read(fnm_in,"pp",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,rf)
              !rf = reshape(tmp2,shape(rf))
              call nc_read(fnm_in,"sp",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,sp)
              !sp = reshape(tmp2,shape(sp))
              !! workaround because data only available as surface thermal/shortwave radiation
              !! lwd = str+lwu
              call nc_read(fnm_in,"str",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,str)
              !str = reshape(tmp2,shape(lwd))
              call nc_read(fnm_in,"tcc",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,tcc)
              call nc_read(fnm_in,"ts",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,ts)
              lwd = 5.31d-13*tt**6 + 60.d0*tcc
              call nc_read(fnm_in,"ssr",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,ssr)
              !ssr = reshape(tmp2,shape(swd))
              call nc_read(fnm_in,"alb",tmp2)
              call interp(tmp2,nx,ny,ntin,ntout,alb)
              swd = ssr/(1.-alb)
          end if
  
      end subroutine read_forcing_loveclim
  
      !> Interpolation routine, used in surface_driver::read_forcing_loveclim and surface_driver::read_validation_loveclim.
      !! Calls spline interpolation (interpolation::spline)
      subroutine interp(tmp,nx,ny,ntin,ntout,vout)
          integer :: nx, ny, ntin, ntout, i, j
          double precision :: vout(nx*ny,ntout)
          double precision :: tmp(nx,ny,ntin), tmp3(nx,ny,ntin+2), tmp_intp(nx,ny,ntout)
          double precision, dimension(ntin+2) :: xi, yi
          double precision, dimension(:), allocatable :: xout, yout
          double precision :: dx
  
          dx = ntout/ntin
          allocate(xout(ntout+int(dx)-1))
          allocate(yout(ntout+int(dx)-1))
  
          do i=1,ntin+2
              xi(i) = (i-1)*dx - dx/2.
          end do
          tmp3(:,:,1) = tmp(:,:,ntin)
          tmp3(:,:,2:ntin+1) = tmp(:,:,:)
          tmp3(:,:,ntin+2) = tmp(:,:,1)
          do i=1,nx
              do j=1,ny
                  yi = tmp3(i,j,:)
                  call spline(ntin+1,ntout+int(dx),xi,yi,xout,yout)
                  tmp_intp(i,j,:) = yout(int(dx/2.)+1:ntout+int(dx/2))
              end do
          end do
          vout = reshape(tmp_intp,shape(vout))
      end subroutine interp
  
  
      !> Read all necessary validation fields from netCDF input file (LOVECLIM).
      subroutine read_validation_loveclim(fnm_in,nx,ny,ntin,ntout,tsurf,alb)
          ! Read in data from netCDF file which contains the fields
          ! to validate the model to. Conform output to 2 dimensions.
  
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: ny !< y dimension
          integer, intent(in) :: ntin !< input time dimension
          integer, intent(in) :: ntout !< output time dimension
          character(len=256), intent(in) :: fnm_in !< LOVECLIM file
          double precision, dimension(nx*ny,ntout), intent(out) :: tsurf !< surface temperature
          double precision, dimension(nx*ny,ntout), intent(out) :: alb !< surface albedo
  
          double precision :: tmp(nx,ny,ntin)
  
          ! Read arrays into validation fields
          if (ntin == ntout) then ! do nothing special
              call nc_read(fnm_in,"ts",tmp)
              tsurf = reshape(tmp,shape(tsurf))
              call nc_read(fnm_in,"alb",tmp)
              alb = reshape(tmp,shape(alb))
          else
              call nc_read(fnm_in,"ts",tmp)
              call interp(tmp,nx,ny,ntin,ntout,tsurf)
              !tsurf = reshape(tmp,shape(tsurf))
              call nc_read(fnm_in,"alb",tmp)
              call interp(tmp,nx,ny,ntin,ntout,alb)
              !alb = reshape(tmp,shape(alb))
          end if
  
      end subroutine read_validation_loveclim
  
  
      !> Read file containing basin enumeration (2D field)
      subroutine read_basin_mask(fnm,nx,ny,basin)
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: ny !< y dimension
          character(len=256), intent(in) :: fnm !< basin file name
          double precision :: tmp(nx,ny)
          integer, intent(out) :: basin(nx*ny) !< basin enumeration
  
          call nc_read(fnm,"MSK",tmp)
          basin = int(reshape(tmp,shape(basin))) - 1
      end subroutine read_basin_mask
  
      !> Read file containing ocean, land, and ice mask (0, 1, 2)
      subroutine read_land_mask(fnm,nx,ny,land_mask)
          integer, intent(in) :: nx !< x dimension
          integer, intent(in) :: ny !< y dimension
          character(len=256), intent(in) :: fnm !< land mask file name
          double precision :: tmp(nx,ny)
          double precision :: mask(nx*ny)
          integer, intent(out) :: land_mask(nx*ny) !< land mask
  
          call nc_read(fnm,"SOL",tmp)
          mask = reshape(tmp,shape(mask))
          land_mask = 0
          where (int(mask) .eq. 11)
              land_mask = 1
          else where (int(mask) .eq. 12)
              land_mask = 2
          end where
      end subroutine read_land_mask
  
end module
