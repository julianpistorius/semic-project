!> Interpolation for 1D (e.g., time series)
module interpolation
    implicit none
contains
    !> Spline interpolation from http://www.physics.unlv.edu/~pang/comp6/spline.f90
    subroutine spline(nt,mt,xi,yi,xout,yout)
        integer, intent(in) :: nt, mt
        double precision, dimension(nt+1), intent(in) :: xi, yi
        double precision, dimension(mt-1), intent(out) :: xout, yout
        integer :: i, k
        double precision, dimension(nt+1) :: p2
        double precision :: h, x, dx, alpha, beta, gamma, eta, f

        call cubic_spline(nt, xi, yi, p2)
        
        h = (xi(nt+1)-xi(1))/mt
        x = xi(1)
        do i = 1, mt-1
            x = x + h
            !
            ! find the interval that x resides
            k = 1
            dx = x-xi(1)
            do while (dx .ge. 0)
                k = k + 1
                dx = x-xi(k)
            end do
            k = k - 1
            !
            ! find the value of function f(x)
            dx = xi(k+1) - xi(k)
            alpha = p2(k+1)/(6*dx)
            beta = -p2(k)/(6*dx)
            gamma = yi(k+1)/dx - dx*p2(k+1)/6
            eta = dx*p2(k)/6 - yi(k)/dx
            f = alpha*(x-xi(k))*(x-xi(k))*(x-xi(k)) &
                +beta*(x-xi(k+1))*(x-xi(k+1))*(x-xi(k+1)) &
                +gamma*(x-xi(k))+eta*(x-xi(k+1))
            xout(i) = x
            yout(i) = f
        end do


    end subroutine spline

    !> \cond not used!
    subroutine cubic_spline (n, xi, fi, p2)
        !
        ! function to carry out the cubic-spline approximation
        ! with the second-order derivatives returned.
        !
        integer :: i
        integer, intent (in) :: n
        double precision, intent (in), dimension (n+1):: xi, fi
        double precision, intent (out), dimension (n+1):: p2
        double precision, dimension (n):: g, h
        double precision, dimension (n-1):: d, b, c
        !
        ! assign the intervals and function differences
        !
        do i = 1, n
            h(i) = xi(i+1) - xi(i)
            g(i) = fi(i+1) - fi(i)
        end do
        !
        ! evaluate the coefficient matrix elements
        do i = 1, n-1
            d(i) = 2*(h(i+1)+h(i))
            b(i) = 6*(g(i+1)/h(i+1)-g(i)/h(i))
            c(i) = h(i+1)
        end do
        !
        ! obtain the second-order derivatives
        !
        call tridiagonal_linear_eq (n-1, d, c, c, b, g)
        p2(1) = 0
        p2(n+1) = 0
        do i = 2, n 
            p2(i) = g(i-1)
        end do
    end subroutine cubic_spline
    !
    subroutine tridiagonal_linear_eq (l, d, e, c, b, z)
        !
        ! functione to solve the tridiagonal linear equation set.
        !
        integer, intent (in) :: l
        integer :: i
        double precision, intent (in), dimension (l):: d, e, c, b
        double precision, intent (out), dimension (l):: z
        double precision, dimension (l):: y, w
        double precision, dimension (l-1):: v, t
        !
        ! evaluate the elements in the lu decomposition
        !
        w(1) = d(1)
        v(1)  = c(1)
        t(1)  = e(1)/w(1)
        do i = 2, l - 1
            w(i) = d(i)-v(i-1)*t(i-1)
            v(i) = c(i)
            t(i) = e(i)/w(i)
        end do
        w(l) = d(l)-v(l-1)*t(l-1)
        !
        ! forward substitution to obtain y
        !
        y(1) = b(1)/w(1)
        do i = 2, l
            y(i) = (b(i)-v(i-1)*y(i-1))/w(i)
        end do
        !
        ! backward substitution to obtain z
        z(l) = y(l)
        do i = l-1, 1, -1
            z(i) = y(i) - t(i)*z(i+1)
        end do
    end subroutine tridiagonal_linear_eq
    !> \endcond not used!
end module
