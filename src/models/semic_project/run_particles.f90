program optimize

    use mpi
    use surface_driver
    use utils

    implicit none
    integer :: y, d, i, i0, i1, blen
    integer :: ierr, num_procs, my_id
    type(smb_class)   :: smb1
    character(len=256) :: nml_file, arg, prefix, output
    logical :: file_exists
    real :: start, finish

    double precision, allocatable, dimension(:) :: cost_stt, cost_smb, cost_swnet, &
                                                   cost_melt, cost_shf, cost_lhf, &
                                                   cost_thf, cost_refr, cost_alb

    ! read namelist filename
    call getarg(1, arg)

    smb1%nml_filename = trim(arg)
    ! read number of startfile
    call getarg(2,arg)
    read(arg,*) prefix
    call getarg(3,arg)
    read(arg,*) i0
    call getarg(4,arg)
    read(arg,*) i1

    call MPI_INIT ( ierr )
    !     find out MY process ID, and how many processes were started.

    call MPI_COMM_RANK (MPI_COMM_WORLD, my_id, ierr)
    call MPI_COMM_SIZE (MPI_COMM_WORLD, num_procs, ierr)


    !do i=0,num_procs-1
    !    if (i == my_id) call sleep(my_id*10)
    !end do
    call smb_init(smb1,"test",0)
    call smb_init_forcing(smb1)

    blen = int(maxval(smb1%basin(smb1%idx)))
    allocate(cost_stt(blen))
    allocate(cost_smb(blen))
    allocate(cost_swnet(blen))
    allocate(cost_melt(blen))
    allocate(cost_refr(blen))
    allocate(cost_shf(blen))
    allocate(cost_lhf(blen))
    allocate(cost_thf(blen))
    allocate(cost_alb(blen))

    ! read through all name lists in ~/data/tmp
    do i = i0+my_id,i1,num_procs
        call cpu_time(start)
        write(nml_file,"(A,I6.6,A)") trim(prefix), i, ".nml"
        inquire(file=nml_file, exist=file_exists)
        if (.not. file_exists) then
            write(*,*) "file '", trim(nml_file), "' not found, exiting loop"
            ! STOP PROGRAM if file list ends (is there a better way?)
            !stop 0
            exit
        end if
        !write(*,*) "\x1B[32msmb_optimize:\x1B[0m read file:", trim(nml_file)
        call smb_reset(smb1,nml_file,nml_file)
        ! open file for CRMSD output
        write(output,"(A,I6.6,A)") trim(prefix), i,  ".out"
        open(2,file=trim(output),form='formatted')
        write(*,"(A,I3.3,A)") "\x1B[32msemic_optimize:\x1B[0m loop ",smb1%nloop, " times over forcing fields"
        do y = 1, smb1%nloop

            ! Daily time step
            do d = 1, smb1%driver%ntime

                ! Calculate SMB 
                call smb_update(smb1,d,y)

            end do
        end do
        cost_stt   = calculate_cost(smb1%state%tsurf(smb1%idx,:),smb1%vali%tsurf(smb1%idx,:),&
                                    smb1%basin(smb1%idx),smb1%surface%par%nx,smb1%driver%ntime,.true.)
        cost_melt  = calculate_cost(smb1%state%melt(smb1%idx,:),smb1%vali%melt(smb1%idx,:),&
                                    smb1%basin(smb1%idx),smb1%surface%par%nx,smb1%driver%ntime,.true.)
        cost_refr  = calculate_cost(smb1%state%refr(smb1%idx,:),smb1%vali%refr(smb1%idx,:),&
                                    smb1%basin(smb1%idx),smb1%surface%par%nx,smb1%driver%ntime,.true.)
        cost_smb   = calculate_cost(smb1%state%massbal(smb1%idx,:),smb1%vali%massbal(smb1%idx,:),&
                                    smb1%basin(smb1%idx),smb1%surface%par%nx,smb1%driver%ntime,.true.)
        cost_swnet = calculate_cost(smb1%state%swnet(smb1%idx,:),smb1%vali%swnet(smb1%idx,:),&
                                    smb1%basin(smb1%idx),smb1%surface%par%nx,smb1%driver%ntime,.true.)
        cost_alb   = calculate_cost(smb1%state%alb(smb1%idx,:),smb1%vali%alb(smb1%idx,:),&
                                    smb1%basin(smb1%idx),smb1%surface%par%nx,smb1%driver%ntime,.true.)
        !cost_shf   = calculate_cost(smb1%state%shf,smb1%vali%shf,smb1%basin,smb1%surface%par%nx,smb1%driver%ntime,.true.)
        !cost_lhf   = calculate_cost(smb1%state%lhf,smb1%vali%lhf,smb1%basin,smb1%surface%par%nx,smb1%driver%ntime,.true.)
        cost_thf   = calculate_cost(smb1%state%lhf(smb1%idx,:)+smb1%state%shf(smb1%idx,:),&
                                    smb1%vali%lhf(smb1%idx,:)+smb1%vali%shf(smb1%idx,:),&
                                    smb1%basin(smb1%idx),smb1%surface%par%nx,smb1%driver%ntime,.true.)
        !write(2,*) cost_stt, cost_melt, cost_smb, cost_swnet
        write(2,*) total_cost(cost_stt, cost_melt, cost_refr, cost_smb, cost_swnet, cost_shf, cost_lhf, cost_thf, cost_alb, blen)
        close(2)
        call cpu_time(finish)
        write(*,"(A,F6.3,A)") "\x1B[32msemic_optimize:\x1B[0m Time = ",finish-start," seconds."
        call smb_output(smb1,1)
    end do

    call smb_end(smb1)

    call MPI_FINALIZE ( ierr )

contains 
    function total_cost(stt, melt, refr, smb, swnet, shf, lhf, thf, alb, b) result(cost)
        integer, intent(in) :: b
        double precision, dimension(b), intent(in) :: stt, melt, refr, smb, swnet, shf, lhf, thf, alb
        double precision :: cost
        integer :: i

        cost = 0.0

        do i=1,b
            !cost = cost + smb(i)**2 + swnet(i)**2
            !cost = cost + shf(i)**2 + lhf(i)**2! + smb(i)**2 + melt(i)**2 + swnet(i)**2 + stt(i)**2
            !cost = cost + thf(i)**2 + smb(i)**2 + melt(i)**2 + swnet(i)**2 + stt(i)**2
            cost = cost + smb(i)**2 + melt(i)**2 + swnet(i)**2 + stt(i)**2
        end do

        cost = dsqrt(cost)
        if (b < 20) then
            write(*,"(A,9F7.3)") 'thf   ', thf
            write(*,"(A,9F7.3)") 'refr  ', refr
            !write(*,"(A,9F7.2)") 'shf   ', shf
            !write(*,"(A,9F7.2)") 'lhf   ', lhf
            write(*,"(A,9F7.3)") 'smb   ', smb
            write(*,"(A,9F7.3)") 'melt  ', melt
            write(*,"(A,9F7.3)") 'swnet ', swnet
            write(*,"(A,9F7.3)") 'stt   ', stt
            write(*,"(A,9F7.3)") 'alb   ', alb
        end if
        write(*,"(A,F7.3)")  'cost  ', cost

    end function

end program optimize 
