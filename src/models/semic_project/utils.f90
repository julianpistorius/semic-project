!> Utility routines such as averages, RMSE calculation, cost functions, etc.
module utils

    use iso_fortran_env, only: int64
    implicit none

    private
    public avg_over_mask, rmse, get_region, rmse_region, centered_rmse, calculate_cost, &
           init_random_seed

contains

    !> Calculate averages over an integer mask
    subroutine avg_over_mask(var, mask, avg)
        double precision, intent(in) :: var(:) !< input vector
        double precision, intent(out) :: avg !< average value
        logical, intent(in) :: mask(:) !< mask vector

        avg = sum(pack(var,mask))/size(pack(var,mask))
    end subroutine avg_over_mask

    subroutine rmse(x,res)
        double precision, intent(in)  :: x(:)
        double precision, intent(out) :: res

        res = sqrt( sum(x**2)/size(x))
    end subroutine

    subroutine nrmse(x,res)
        double precision, intent(in)  :: x(:)
        double precision, intent(out) :: res

        res = sqrt( sum(x**2)/size(x))/(maxval(x)-minval(x))
    end subroutine

    subroutine get_region(var_in,mask,var_out)
        double precision, intent(in) :: var_in(:)
        logical, intent(in) :: mask(:)
        double precision, intent(out) :: var_out(:)
        var_out = pack(var_in,mask)
    end subroutine get_region

    !> Calculated the root-mean-square-error (RMSE) of a variable using a specified mask
    subroutine rmse_region(var_in,mask,rmse_out)
        double precision, intent(in) :: var_in(:) !< input vector
        logical, intent(in) :: mask(:) !< mask vector
        double precision, intent(out) :: rmse_out !< calculated RMSE
        double precision, allocatable :: region(:)
        integer :: xsize

        xsize = int(sum(var_in*0.0+1.0,mask=mask))
        allocate(region(xsize))
        call get_region(var_in,mask,region)
        call rmse(region,rmse_out)
    end subroutine rmse_region

    function centered_rmse(x1,x2,nx,scaled) result(res)
        integer, intent(in) :: nx
        double precision, intent(in), dimension(nx) :: x1, x2
        double precision :: x1_avg, x2_avg, x1_std, x2_std
        double precision :: crmse, res
        logical, optional, intent(in) :: scaled
        logical :: scale_me
        if(.not. present(scaled) ) then
            scale_me = .false.
        else
            scale_me = scaled
        end if

        x1_avg = sum(x1)/nx
        x2_avg = sum(x2)/nx
        x2_std = dsqrt(sum((x2 - x2_avg)**2)/nx)
        x1_std = dsqrt(sum((x1 - x1_avg)**2)/nx)
        if (scale_me) then
            crmse = dsqrt(sum(((x1 - x1_avg)-(x2 - x2_avg))**2)/nx)/x2_std
            !res = dsqrt(crmse**2 + (x1_std/x2_std - 1.0)**2)
            res = dsqrt(crmse**2 + ((x1_avg-x2_avg)/x2_std)**2)
        else
            !crmse = dsqrt(sum(((x1/x1_avg - 1.0)-(x2/x2_avg - 1.0))**2)/nx)
            res = dsqrt(sum(((x1/x1_avg - 1.0)-(x2/x2_avg - 1.0))**2)/nx + (x1_std - x2_std)**2)
        end if
    end function

    function calculate_cost(x1,x2,mask,nsize,ntime,scaled,cumulative) result(cost)
        integer, intent(in) :: nsize, ntime
        double precision, dimension (nsize,ntime), intent(in) :: x1, x2
        integer, dimension (nsize), intent(in) :: mask
        double precision, allocatable :: cost(:)
        double precision, dimension(ntime) :: x1_avg, x2_avg
        logical, dimension(nsize) :: lmask
        logical, optional, intent(in) :: scaled, cumulative
        logical :: is_cumulative
        integer :: t, m, blen
        double precision :: xsize, totsize

        if(.not. present(cumulative) ) then
            is_cumulative = .false.
        else
            is_cumulative = cumulative
        end if
        
        blen = maxval(mask)

        if (.not. allocated(cost)) allocate(cost(blen))

        totsize = real(sum(x1(:,1)*0.0+1.0))
        do m=1,blen
            where(mask.eq.m)
                lmask = .true.
            elsewhere
                lmask = .false.
            end where
            xsize = real(sum(x1(:,1)*0.0+1.0,mask=lmask))
            do t=1,ntime
                if (xsize <= 1.0) then
                    x1_avg(t) = x1(1,t)
                    x2_avg(t) = x2(1,t)
                else
                    call avg_over_mask(x1(:,t),lmask,x1_avg(t))
                    call avg_over_mask(x2(:,t),lmask,x2_avg(t))
                end if

                if (t>1 .and. is_cumulative) then
                    x1_avg(t) = xsize*x1_avg(t) + x1_avg(t-1)
                    x2_avg(t) = xsize*x2_avg(t) + x2_avg(t-1)
                end if
            end do
    
            cost(m) = centered_rmse(x1_avg,x2_avg,ntime,scaled)*xsize/totsize
        end do

    end function
    
    
    subroutine init_random_seed()
    ! from https://gcc.gnu.org/onlinedocs/gfortran/RANDOM_005fSEED.html#RANDOM_005fSEED
#ifdef USE_INTEL
      use ifport
#endif
      integer, allocatable :: myseed(:)
      integer :: i, n, un, istat, dt(8), pid
      integer(int64) :: t
    
      call random_seed(size = n)
      allocate(myseed(n))
      ! First try if the OS provides a random number generator
      open(newunit=un, file="/dev/urandom", access="stream", &
           form="unformatted", action="read", status="old", iostat=istat)
      if (istat == 0) then
         read(un) myseed
         close(un)
      else
         ! Fallback to XOR:ing the current time and pid. The PID is
         ! useful in case one launches multiple instances of the same
         ! program in parallel.
         call system_clock(t)
         if (t == 0) then
            call date_and_time(values=dt)
            t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
                 + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
                 + dt(3) * 24_int64 * 60 * 60 * 1000 &
                 + dt(5) * 60 * 60 * 1000 &
                 + dt(6) * 60 * 1000 + dt(7) * 1000 &
                 + dt(8)
         end if
         pid = getpid()
         t = ieor(t, int(pid, kind(t)))
         do i = 1, n
            myseed(i) = lcg(t)
         end do
      end if
      call random_seed(put=myseed)
    contains
      ! This simple PRNG might not be good enough for real work, but is
      ! sufficient for seeding a better PRNG.
      function lcg(s)
        integer :: lcg
        integer(int64) :: s
        if (s == 0) then
           s = 104729
        else
           s = mod(s, 4294967296_int64)
        end if
        s = mod(s * 279470273_int64, 4294967291_int64)
        lcg = int(mod(s, int(huge(0), int64)), kind(0))
      end function lcg
    end subroutine init_random_seed

end module
