from ftplib import FTP
import sys, os
import hashlib

ROOT       = '.'
ftp_server = 'ftp.climato.be'

def download(path,files):
    opath = ROOT+'/data/external'+path

    if not os.path.exists(opath):
        os.makedirs(opath)
    
    ftp = FTP(ftp_server)
    ftp.login()
    ftp.cwd(path)
    for fnm in files:
        # check if file with *.md5 exists 
        if (os.path.isfile(opath+fnm+'.md5')):
            with open(opath+fnm+'.md5', 'r') as md5file:
                md5sum = md5file.read().replace('\n', '')
        else:
            md5sum = None
        # compare MD5 hash
        if (os.path.isfile(opath+fnm) and hashlib.md5(open(opath+fnm, 'rb').read()).hexdigest() == md5sum):
            print 'File %s exists.' % fnm
        else:
    	    print 'Download file %s from %s to %s' % (fnm, ftp_server, opath)
            # Download from FTP server if file does not exist, yet
    	    ftp.retrbinary('RETR %s' % fnm, open(opath+fnm, 'wb').write)
            # Write MD5 checksum to file
            with open(opath+fnm+'.md5', 'w') as md5file:
                md5file.write(hashlib.md5(open(opath+fnm, 'rb').read()).hexdigest())
    ftp.quit()

if sys.argv[1] == 'all':
    # MARv2 historical data
    download('/fettweis/MARv2/MARv2-CanESM2-histo/',['MARv2-CanESM2-histo-%d.nc' %i for i in range(1970,2006)])
    # MARv2 RCP8.5 data
    download('/fettweis/MARv2/MARv2-CanESM2-rcp85/',['MARv2-CanESM2-rcp85-%d.nc' %i for i in range(2006,2101)])
if sys.argv[1] == 'forcing':
    # MARv2 historical data
    download('/fettweis/MARv2/MARv2-CanESM2-histo/',['MARv2-CanESM2-histo-%d.nc' %i for i in range(1990,2000)])
    # MARv2 RCP8.5 data
    download('/fettweis/MARv2/MARv2-CanESM2-rcp85/',['MARv2-CanESM2-rcp85-%d.nc' %i for i in range(2090,2100)])
