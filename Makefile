.PHONY: clean semic docs get_data historical rcp85

f_histo = "data/external/fettweis/MARv2/MARv2-CanESM2-histo/MARv2-CanESM2-histo"
f_rcp85 = "data/external/fettweis/MARv2/MARv2-CanESM2-rcp85/MARv2-CanESM2-rcp85"
params = "MSK,SOL,UU,VV,TT,QQ,SF,RF,SP,LWD,SWD,STT,ME,RZ,SMB,AL1,SHF,LHF,SHSN3,TTMIN,TTMAX,LON,LAT"

debug ?= 0
ifort ?= 0

get_data:
	python src/data/get_MAR_data.py all

forcing:
	python src/data/get_MAR_data.py forcing
	mkdir -p data/interim
	cdo -s -O -f nc -mergetime \
		-selname,$(params) $(f_rcp85)-2090.nc \
		-selname,$(params) $(f_rcp85)-2091.nc \
		-selname,$(params) $(f_rcp85)-2092.nc \
		-selname,$(params) $(f_rcp85)-2093.nc \
		-selname,$(params) $(f_rcp85)-2094.nc \
		-selname,$(params) $(f_rcp85)-2095.nc \
		-selname,$(params) $(f_rcp85)-2096.nc \
		-selname,$(params) $(f_rcp85)-2097.nc \
		-selname,$(params) $(f_rcp85)-2098.nc \
		-selname,$(params) $(f_rcp85)-2099.nc \
		data/interim/forcing_2090-2099.nc
	cdo -s -timmean data/interim/forcing_2090-2099{,_am}.nc
	cdo -s -O -f nc -mergetime \
		-selname,$(params) $(f_histo)-1990.nc \
		-selname,$(params) $(f_histo)-1991.nc \
		-selname,$(params) $(f_histo)-1992.nc \
		-selname,$(params) $(f_histo)-1993.nc \
		-selname,$(params) $(f_histo)-1994.nc \
		-selname,$(params) $(f_histo)-1995.nc \
		-selname,$(params) $(f_histo)-1996.nc \
		-selname,$(params) $(f_histo)-1997.nc \
		-selname,$(params) $(f_histo)-1998.nc \
		-selname,$(params) $(f_histo)-1999.nc \
		data/interim/forcing_1990-1999.nc
	cdo -s -O -timmean data/interim/forcing_1990-1999{,_am}.nc

diurnal_cycle:
	cdo -s -O -merge -selname,LON,LAT data/interim/forcing_1990-1999.nc -yseasmean -sub -selname,TTMAX data/interim/forcing_1990-1999.nc -selname,TTMIN data/interim/forcing_1990-1999.nc data/interim/diurnal_cycle.nc

historical: semic config/semic-histo.nml
	./bin/semic.x config/semic-histo.nml
	cdo -s yearmean data/raw/semic_historical_ts.nc data/processed/semic_historical_ts_ym.nc

rcp85: semic config/semic-rcp85.nml
	./bin/semic.x config/semic-rcp85.nml
	cdo -s yearmean data/raw/semic_rcp85_ts.nc data/processed/semic_rcp85_ts_ym.nc

run: semic config/semic_1990-1999.nml config/semic_2090-2099.nml
	mkdir -p data/raw
	mkdir -p data/processed
	./bin/semic.x config/semic_1990-1999.nml
	cdo -s timmean data/raw/semic_1990-1999_opti_d.nc data/processed/semic_1990-1999_opti_d_am.nc
	./bin/semic.x config/semic_2090-2099.nml
	cdo -s timmean data/raw/semic_2090-2099_opti_d.nc data/processed/semic_2090-2099_opti_d_am.nc

semic:
	cd src && debug=$(debug) ifort=$(ifort) make semic

clean:
	cd src && make clean
	cd reports && make clean

# default parameters for optimization tools
method ?= pso
ntopo ?= 1000
max_iter ?= 50

optimize: semic
	mkdir -p data/interim
	python src/tools/optimization/optimization.py $(method)

sensitivity:
	mkdir -p data/raw/sensitivity
	python src/tools/optimization/sensitivity.py $(max_iter)

topology:
	mkdir -p data/interim
	python src/tools/optimization/topology.py $(ntopo)

comparison:
	./src/tools/comparison.sh "1990-1999"
	./src/tools/comparison.sh "2090-2099"

figures:
	python src/visualization/figures.py
	cd reports && latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make figures.tex
	pdfseparate -f 1 -l 1 reports/figures.pdf reports/figures/fig07.pdf
	pdfseparate -f 2 -l 2 reports/figures.pdf reports/figures/fig09.pdf
	pdfseparate -f 3 -l 3 reports/figures.pdf reports/figures/fig10.pdf

paper:
	cd reports && make clean && make

save_data:
	cd data && find ./{raw,processed} -type f \( -name "*.nc" -o -name "*.nml" -o -name "*.out" \) | tar cfvz semic_data_$(shell date +%Y%m%d).tar.gz -T -
