# SEMIC Project Description

This is the technical description of the
*Surface Energy and Mass Balance Model of Intermediate Complexity* (*SEMIC*)
and its application to the Greenland Ice Sheet.

## Project Organization

    .
    ├── AUTHORS.md
    ├── LICENSE
    ├── README.md
    ├── bin
    ├── config
    ├── data
    │   ├── external
    │   │   └── fettweis
    │   │       └── MARv2
    │   │           ├── MARv2-CanESM2-histo
    │   │           └── MARv2-CanESM2-rcp85
    │   ├── interim
    │   ├── processed
    │   └── raw
    ├── reports
    │   └── figures
    └── src
        ├── data
        ├── example
        ├── external
        │   ├── coordinates-0.1
        │   ├── ncio-1.1
        │   └── semic-1.2
        ├── models
        │   └── semic_project
        ├── tools
        │   └── optimization
        └── visualization

# Running different instances of the model framework

## Prerequisites

### Python libraries

Many tools for data analysis and visualization are written in Python.
Therefore, the following Python packages need to be installed

* `numpy`
* `scipy`
* `netCDF4`
* `matplotlib` including the [Basemap Toolkit](http://matplotlib.org/basemap/)

Usually these libraries can be installed with `pip install <package>`.

### Machine-specific settings

On the PIK cluster for *GNU* Fortran
```
module load compiler/gnu/4.9.2
module load netcdf-fortran/4.4.2/gnu/serial
module load netcdf-c/4.3.3.1/gnu/serial
module load cdo/1.7.1
```

or for *Intel* Fortran
```
module load compiler/intel/16.0.0
module load netcdf-fortran/4.4.2/intel/16.0.0/serial
module load netcdf/4.1.3/intel/16.0.0/serial 
module load cdo/1.7.1
ulimit -s unlimited # without, you'll likely get this error:
# forrtl: severe (174): SIGSEGV, segmentation fault occurred
```

Prepend `ifort=1` to the `make ...` commands below to compile the model with
the *Intel* Fortran compiler.

### The model code

First, we need to build or model and download the data, to which we want
to compare our results and from which we need the input to our *SEMIC* model.
We need to build two binaries: one for normal model runs (**`semic.x`**)
and one for the optimization and sensitivity (**`run_particles.x`**).
MPI is supported to allow multiple model instances to run in parallel
on multi-core machines.

Build binaries via:
```
make semic
```

In this step several external model sources are compiled 
(needed by the model framework): [`ncio`](https://github.com/alex-robinson/ncio),
a simple Fortran interface to NetCDF reading and writing,
[`coordinates`](https://github.com/alex-robinson/coordinates),
a module to handle grid/points definition, interpolation mapping and subsetting,
and finally, [`semic`](https://github.com/mkrapp/semic),
the actual surface and energy balance model but *not* the framework.

### The input/validation data

We use output data from the regional climate model MAR
(download from [FTP server](ftp://ftp.climato.be/fettweis/MARv2)):
```
make get_data
```

The data is stored in `data/external/fettweis`

**This may take a while and takes about 80Gb of space!**

To create a forcing data set from the downloaded data
(covering the years 1990-1999 from the historical period and the years 2090-2099 from an RCP8.5 scenario), type
```
make forcing
```

This creates a new NetCDF file `data/interim/forcing_1990-1999.nc` and `data/interim/forcing_2090-2099.nc`.

**For testing the framework, you can also just download the compiled files `forcing_1990-1999.nc` and `forcing_2090-2099.nc` download the file from our [Open Science Framework project site](https://osf.io/a3vh2/) and move the files to `data/interim/`. (You will are not be able to run the *historical* and *RCP8.5* scenarios; you need the full data set to do so.)**

Run *SEMIC* in normal mode with parameters defined in namelist `config/semic_1990-1999.nml` and `config/semic_2090-2099.nml`
and  input data from the file `data/interim/forcing_1990-1999.nc` and `data/interim/forcing_2090-2099.nc`:
```
make run
```

In the final version of this project the parameters in `config/surface_physics.nml` are
the optimal parameters as obtained by the parameter optimization (see below).
Therefore, the model output from the above model run can be regarded as *optimal*.

## Run the model for the historical period and the RCP8.5 projection

To validate the performance of *SEMIC* we make a comparison with actual *MAR*
runs for the historical period and the projection for the RCP8.5 scenario.

```
make historical
make rcp85
```

*Those results are also part of our paper.*

## Create figures

Figures from the manuscript "*SEMIC*: An efficient surface energy and mass balance model applied to the Greenland Ice Sheet" can be created via

```
make figures
```

(Before that you should type `make diurnal_cycle` to create the file for the corresponding figure.)

## Save data

```
make save_data
```
will put the raw and processed data into a tar file `data/semic_data_<timestamp>.tar.gz` (e.g., for archiving with a time stamp).

## Run parameter optimization (expert-only)

To run the optimization, first install optimization algorithms (Python package)
```
cd src
make install-optimization
cd ..
make optimize
```

The default optimization technique used here is the so-called
*Particle Swarm Optimization*, often used in non-linear optimization problems.
Optionally, prepend the method to be used, e.g., `method=es make optimize`
(that is for *Evolution Strategies*)

For the optimization you need to create a random sub-sample forcing data set (to reduce compuational time)

```
python src/tools/random_forcing_subsampling.py <N> <file.nc>
```

Here, `N` is the number of data points skipped, i.e., every `N`th data point is randomly chosen.
Note that you need to manually change the file `surface_driver.f90` to adjust to your new data set (if `N`=8 and the output NetCDF file is `./data/interim/forcing_1990-1999-2090-2099_sub_sample.nc`, then no changes need to be made).

*Those results are also part of our paper.*

Once the optimization is finsihed you have to manually changes the parameter
values in `config/surface_physics.nml` with the values from the optimization:

## Run parameter sensitivity (expert-only)

The optimization is based on finding the optimal parameters within the limits
given in file `config/namelist.ranges`. To run the parameter sensitivity,
simply type
```
[max_iter=N] make sensitivity
```

with `max_iter` being the number of samples drawn from the
parameter range (equally-spaced).   

*Those results are also part of our paper.*

- - -

If you like how this research project has been structured, feel free to start
your next one with
[Cookiecutter for Reproducible Science](https://github.com/mkrapp/cookiecutter-reproducible-science).
